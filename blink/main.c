
#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>

//defines needed for Atmega328: 
#define    UCSRA    UCSR0A 
#define    UCSRB    UCSR0B 
#define    UCSRC    UCSR0C 
#define    UBRRH    UBRR0H 
#define    UBRRL    UBRR0L 
#define    UDRE    UDRE0 
#define    UDR    UDR0 
#define    RXC    RXC0 
#define    RXEN    RXEN0 
#define    TXEN    TXEN0 
#define    UCSZ1    UCSZ01 
#define    UCSZ0    UCSZ00 
#define output_high(port,pin) port |= (1<<pin) 
#define output_low(port,pin) port &= ~(1<<pin) 
#define set_output(portdir,pin) portdir |= (1<<pin) 

    static int uart_putchar(char c, FILE *stream);

    static FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, NULL,
                                             _FDEV_SETUP_WRITE);

    static int
    uart_putchar(char c, FILE *stream)
    {

      if (c == '\n')
        uart_putchar('\r', stream);
      loop_until_bit_is_set(UCSR0A, UDRE0);
      UDR0 = c;
      return 0;
	}
	
void InitUART (unsigned char baudrate){ 
  /* Set the baud rate */ 
  UBRRL = baudrate; 
  /* Enable UART receiver and transmitter */ 
  UCSRB = (1 << RXEN) | (1 << TXEN); 
  /* set to 8 data bits, 1 stop bit */ 
  UCSRC = (1 << UCSZ1) | (1 << UCSZ0); 
} 
	
int main()
{
	//InitUART(0x0);
	//stdout = &mystdout;
    DDRB |= _BV(DDB1); 
    
    while(1){
	
        PORTB ^= _BV(PB1);
		//printf("hello world!\n");
        _delay_ms(80);
    }
}