#include <Arduino.h>
#include <twi.h>
//#include <SPI.h>
#include <MCP23017.h>
#include <avr/io.h>

mcp23017_t mcp0;

void setup()  {
DDRB |= (1<<DDB5);
PORTB |= (1<<PORTB5);
twi_init(400000L);
mcp23017_create(&mcp0, 0);


}

uchar x = 0;

void loop()  {
/*uchar bytestosend[2];
bytestosend[0] = MCP23017ADDR_IODIRA;//register addr
bytestosend[1] = 0x00; //all bits as output
twi_writeTo(MCP23017_FIXEDADDR | 0, bytestosend, 2, 0, 1); //send and stop
bytestosend[0] = MCP23017ADDR_IODIRB;//register addr
bytestosend[1] = 0x00; //all bits as output
twi_writeTo(MCP23017_FIXEDADDR | 0, bytestosend, 2, 0, 1); //send and stop
bytestosend[0] = MCP23017ADDR_GPIOA;//register addr
bytestosend[1] = ((char*)&x)[0]; //all bits as output
twi_writeTo(mcp0.raddress, bytestosend, 2, 0, 1); //send and stop
bytestosend[0] = MCP23017ADDR_GPIOB;//register addr
bytestosend[1] = ((char*)&x)[1]; //all bits as output
twi_writeTo(MCP23017_FIXEDADDR | 0, bytestosend, 2, 0, 1); //send and stop
*/
mcp23017_setbyte(&mcp0, MCP23017ADDR_IODIRB, 0xFF);
mcp23017_setbyte(&mcp0, MCP23017ADDR_IODIRA, 0x00);
//mcp23017_setbyte(&mcp0, MCP23017ADDR_GPIOA, ((char*)&x)[0]);
x = mcp23017_getbyte(&mcp0, MCP23017ADDR_GPIOB);
mcp23017_setbyte(&mcp0, MCP23017ADDR_GPIOA, x);
delay(20);
PORTB ^= (1<<PORTB5);
//uchar bytestosend[2];
//bytestosend[0] = MCP23017ADDR_GPIOA;//register addr
//bytestosend[1] = x; //all bits as output
//twi_writeTo(MCP23017_FIXEDADDR | 0, bytestosend, 2, 0, 1); //send and stop
//delay(100);
}
