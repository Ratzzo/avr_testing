#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../../../types/cvector/cvector/cvector.h"

typedef unsigned char uchar;

#define CMDBUFSIZE 256

uchar mgmt_cmdbuf[CMDBUFSIZE];

#define p(...) printf(__VA_ARGS__)

int pinMode(uchar a, uchar b){
printf("pinMode(%i, %i)", a, b);
}

int digitalWrite(uchar a, uchar b){
printf("digitalWrite(%i, %i)", a, b);
}


int commandprocessor(uint argc, uchar *argv[]){
uint i;
p("commnum: %u\n", argc);
for(i=0;i<argc;i++)
p("%s-", argv[i]);
p("\n");

if(!strcmp((char*)argv[0], "pinmode")){ //pinmode command: pinmode <pin> <in | out>
    uint pin = 2;
    uchar mode = 0;
    pin = atoi((char*)argv[1]);
    if(!strcmp((char*)argv[2], "in"))
        mode = 0;
    else if(!strcmp((char*)argv[2], "out"))
        mode = 1;
    else
    {
        p("pinmode <pin> <in | out>\n");
        return 1;
    }
    pinMode(pin, mode);
    return 0;
} else if(!strcmp((char*)argv[0], "dwrite")){ //digitalwrite command: dwrite <pin> <lo | hi>
    uint pin = 2;
    uchar value = 0;
    pin = atoi((char*)argv[1]);
    if(!strcmp((char*)argv[2], "lo"))
        value = 0;
    else if(!strcmp((char*)argv[2], "hi"))
        value = 1;
    else
    {
        p("dwrite <pin> <lo | hi>\n");
        return 1;
    }
    digitalWrite(pin, value);
    }

    else{
    p("command not recognized\n");
    return 1;
    }
return 0;
}

int mgmt_processcommand(uchar *cmdbuf){
cvector arguments_positions;
cvector arguments_sizes;

cvector_allocate(&arguments_positions, sizeof(uchar), 0);
cvector_allocate(&arguments_sizes, sizeof(uchar), 0);

uchar quotations = 0;
uint i;
uchar lastpos = 0;

uchar totalsize;
uchar destindex;

uchar **tempb;
uchar *tempbuff;


for(i=0;i<CMDBUFSIZE;i++){
uchar chari = i;

if(cmdbuf[i] == '"') //toggle if quotation found
quotations ^= 1;

//printf("quotations: %u\n", quotations);


if((cmdbuf[i] == 0 || (cmdbuf[i] == 0x20 && quotations == 0)))  { //check if the ending is 0 or 20 and not into quotations
chari -= lastpos;

if(chari != 0){
cvector_push(&arguments_positions, &lastpos);
cvector_push(&arguments_sizes, &chari);
}

lastpos = i+1;
}


if(cmdbuf[i] == 0 || cmdbuf[i] == '\n' || cmdbuf[i] == 13) break;

//cvector_push(&arguments_positions, &lastpos);
//cvector_push(&arguments_sizes, &chari);

}

if(quotations)
{
p("syntax error\n");
goto syntaxerror1;
}
//size sum
totalsize = 0;
for(i=0;i<arguments_sizes.len;i++){
totalsize += *((uchar*)cvector_getitem(&arguments_sizes,i)) + 1;
}

tempbuff = (uchar*)malloc(totalsize + sizeof(uchar*)*arguments_sizes.len);
memset(tempbuff, 0, totalsize +  sizeof(uchar*)*arguments_sizes.len);
//asm volatile("int3");
destindex = 0;
for(i=0;i<arguments_positions.len;i++){
uchar templen = *((uchar*)cvector_getitem(&arguments_sizes, i));
uchar index = *((uchar*)cvector_getitem(&arguments_positions, i));
tempb = (uchar**)(tempbuff+totalsize);

tempb[i] = tempbuff+destindex;

//if(cmdbuf[index] == '"' && cmdbuf[index+templen-1] == '"')
//memcpy(tempbuff + index, cmdbuf + index + 1, templen - 2);
//else

memcpy(tempbuff + destindex, cmdbuf + index, templen);
destindex += templen+1;
}

//debug
//for(i=0; i < totalsize+1*arguments_sizes.len; i++){
//printf("%u\n", cmdbuf[i]);
//}

commandprocessor(arguments_positions.len, tempb);

syntaxerror1:

free(tempbuff);

cvector_destroy(&arguments_positions);
cvector_destroy(&arguments_sizes);
return 0;
}


int main(){
mgmt_processcommand((uchar*)"hell     \"2 out\"    asd\"");
printf("%u, %u\n", 0 ^ 1, 1 ^ 1 );
}
