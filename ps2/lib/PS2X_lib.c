#include "PS2X_lib.h"
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <avr/io.h>
#include "Arduino.h"

static byte enter_config[]={0x01,0x43,0x00,0x01,0x00};
static byte set_mode[]={0x01,0x44,0x00,0x01,0x03,0x00,0x00,0x00,0x00};
static byte set_bytes_large[]={0x01,0x4F,0x00,0xFF,0xFF,0x03,0x00,0x00,0x00};
static byte exit_config[]={0x01,0x43,0x00,0x00,0x5A,0x5A,0x5A,0x5A,0x5A};
static byte enable_rumble[]={0x01,0x4D,0x00,0x00,0x01};
static byte type_read[]={0x01,0x45,0x00,0x5A,0x5A,0x5A,0x5A,0x5A,0x5A};

#ifdef __AVR__
    inline void CLK_SET(PS2X_t *ps);
    inline void CLK_CLR(PS2X_t *ps);
    inline void CMD_SET(PS2X_t *ps);
    inline void CMD_CLR(PS2X_t *ps);
    inline void ATT_SET(PS2X_t *ps);
    inline void ATT_CLR(PS2X_t *ps);
    inline boolean DAT_CHK(PS2X_t *ps);
#endif

unsigned char _ps_gamepad_shiftinout (PS2X_t *ps, char byte);
void ps_sendCommandString(PS2X_t *ps, byte*, byte);


/****************************************************************************************/
boolean ps_Newbuttonstate(PS2X_t *ps) {
  return ((ps->last_buttons ^ ps->buttons) > 0);
}

/****************************************************************************************/
boolean ps_Newbuttonstate2(PS2X_t *ps, unsigned int button) {
  return (((ps->last_buttons ^ ps->buttons) & button) > 0);
}

/****************************************************************************************/
boolean ps_ButtonPressed(PS2X_t *ps, unsigned int button) {
  return(ps_Newbuttonstate2(ps, button) & ps_Button(ps, button));
}

/****************************************************************************************/
boolean ps_ButtonReleased(PS2X_t *ps, unsigned int button) {
  return((ps_Newbuttonstate2(ps, button)) & ((~ps->last_buttons & button) > 0));
}

/****************************************************************************************/
boolean ps_Button(PS2X_t *ps, uint16_t button) {
  return ((~ps->buttons & button) > 0);
}

/****************************************************************************************/
unsigned int ps_ButtonDataByte(PS2X_t *ps) {
   return (~ps->buttons);
}

/****************************************************************************************/
byte ps_Analog(PS2X_t *ps, byte button) {
   return ps->PS2data[button];
}

/****************************************************************************************/
unsigned char _ps_gamepad_shiftinout (PS2X_t *ps, char byte) {
   unsigned char tmp = 0;
   unsigned char i;
   for(i=0;i<8;i++) {
      if(CHK(byte,i)) CMD_SET(ps);
      else CMD_CLR(ps);

      CLK_CLR(ps);
      delayMicroseconds(CTRL_CLK);

      //if(DAT_CHK(ps)) SET(tmp,i);
      if(DAT_CHK(ps)) bitSet(tmp,i);

      CLK_SET(ps);
#if CTRL_CLK_HIGH
      delayMicroseconds(CTRL_CLK_HIGH);
#endif
   }
   CMD_SET(ps);
   delayMicroseconds(CTRL_BYTE_DELAY);
   return tmp;
}

/****************************************************************************************/
void ps_read_gamepad(PS2X_t *ps) {
   ps_read_gamepad2(ps, false, 0x00);
}

long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

/****************************************************************************************/
boolean ps_read_gamepad2(PS2X_t *ps, boolean motor1, byte motor2) {
   double temp = millis() - ps->last_read;

   if (temp > 1500) //waited to long
      ps_reconfig_gamepad(ps);

   if(temp < ps->read_delay)  //waited too short
      delay(ps->read_delay - temp);

   if(motor2 != 0x00)
      motor2 = map(motor2,0,255,0x40,0xFF); //noting below 40 will make it spin

   char dword[9] = {0x01,0x42,0,(char)motor1,(char)motor2,0,0,0,0};
   byte dword2[12] = {0,0,0,0,0,0,0,0,0,0,0,0};

   // Try a few times to get valid data...
   byte RetryCnt;
   for (RetryCnt = 0; RetryCnt < 5; RetryCnt++) {
      CMD_SET(ps);
      CLK_SET(ps);
      ATT_CLR(ps); // low enable joystick

      delayMicroseconds(CTRL_BYTE_DELAY);
      //Send the command to send button and joystick data;
      int i;
      for (i = 0; i<9; i++) {
         ps->PS2data[i] = _ps_gamepad_shiftinout(ps, dword[i]);
      }

      if(ps->PS2data[1] == 0x79) {  //if controller is in full data return mode, get the rest of data
        int i;
         for (i = 0; i<12; i++) {
            ps->PS2data[i+9] = _ps_gamepad_shiftinout(ps, dword2[i]);
         }
      }

      ATT_SET(ps); // HI disable joystick
      // Check to see if we received valid data or not.
	  // We should be in analog mode for our data to be valid (analog == 0x7_)
      if ((ps->PS2data[1] & 0xf0) == 0x70)
         break;

      // If we got to here, we are not in analog mode, try to recover...
      ps_reconfig_gamepad(ps); // try to get back into Analog mode.
      delay(ps->read_delay);
   }

   // If we get here and still not in analog mode (=0x7_), try increasing the read_delay...
   if ((ps->PS2data[1] & 0xf0) != 0x70) {
      if (ps->read_delay < 10)
         ps->read_delay++;   // see if this helps out...
   }

#ifdef PS2X_COM_DEBUG
   Serial.println("OUT:IN");
   for(int i=0; i<9; i++){
      Serial.print(dword[i], HEX);
      Serial.print(":");
      Serial.print(PS2data[i], HEX);
      Serial.print(" ");
   }
   for (int i = 0; i<12; i++) {
      Serial.print(dword2[i], HEX);
      Serial.print(":");
      Serial.print(PS2data[i+9], HEX);
      Serial.print(" ");
   }
   Serial.println("");
#endif

   ps->last_buttons = ps->buttons; //store the previous ps->buttons states

#if defined(__AVR__)
   ps->buttons = *(uint16_t*)(ps->PS2data+3);   //store as one value for multiple functions
#else
   ps->buttons =  (uint16_t)(PS2data[4] << 8) + PS2data[3];   //store as one value for multiple functions
#endif
   ps->last_read = millis();
   return ((ps->PS2data[1] & 0xf0) == 0x70);  // 1 = OK = analog mode - 0 = NOK
}

/****************************************************************************************/
byte ps_config_gamepad(PS2X_t *ps, uint8_t clk, uint8_t cmd, uint8_t att, uint8_t dat) {
   return ps_config_gamepad2(ps, clk, cmd, att, dat, false, false);
}

/****************************************************************************************/
byte ps_config_gamepad2(PS2X_t *ps, uint8_t clk, uint8_t cmd, uint8_t att, uint8_t dat, boolean pressures, boolean rumble) {

  byte temp[sizeof(type_read)];

#ifdef __AVR__
  ps->_clk_mask = digitalPinToBitMask(clk);
  ps->_clk_oreg = portOutputRegister(digitalPinToPort(clk));
  ps->_cmd_mask = digitalPinToBitMask(cmd);
  ps->_cmd_oreg = portOutputRegister(digitalPinToPort(cmd));
  ps->_att_mask = digitalPinToBitMask(att);
  ps->_att_oreg = portOutputRegister(digitalPinToPort(att));
  ps->_dat_mask = digitalPinToBitMask(dat);
  ps->_dat_ireg = portInputRegister(digitalPinToPort(dat));
#else
  uint32_t            lport;                   // Port number for this pin
  _clk_mask = digitalPinToBitMask(clk);
  lport = digitalPinToPort(clk);
  _clk_lport_set = portOutputRegister(lport) + 2;
  _clk_lport_clr = portOutputRegister(lport) + 1;

  _cmd_mask = digitalPinToBitMask(cmd);
  lport = digitalPinToPort(cmd);
  _cmd_lport_set = portOutputRegister(lport) + 2;
  _cmd_lport_clr = portOutputRegister(lport) + 1;

  _att_mask = digitalPinToBitMask(att);
  lport = digitalPinToPort(att);
  _att_lport_set = portOutputRegister(lport) + 2;
  _att_lport_clr = portOutputRegister(lport) + 1;

  _dat_mask = digitalPinToBitMask(dat);
  _dat_lport = portInputRegister(digitalPinToPort(dat));
#endif

  pinMode(clk, OUTPUT); //configure ports
  pinMode(att, OUTPUT);
  pinMode(cmd, OUTPUT);
  pinMode(dat, INPUT);

#if defined(__AVR__)
  digitalWrite(dat, HIGH); //enable pull-up
#endif

  CMD_SET(ps); // SET(*_cmd_oreg,_cmd_mask);
  CLK_SET(ps);

  //new error checking. First, read gamepad a few times to see if it's talking
  ps_read_gamepad(ps);
  ps_read_gamepad(ps);

  //see if it talked - see if mode came back.
  //If still anything but 41, 73 or 79, then it's not talking
  if(ps->PS2data[1] != 0x41 && ps->PS2data[1] != 0x73 && ps->PS2data[1] != 0x79){
#ifdef PS2X_DEBUG
    Serial.println("Controller mode not matched or no controller found");
    Serial.print("Expected 0x41, 0x73 or 0x79, but got ");
    Serial.println(ps->PS2data[1], HEX);
#endif
    return 1; //return error code 1
  }

  //try setting mode, increasing delays if need be.
  ps->read_delay = 1;
    int y;
  for(y = 0; y <= 10; y++) {
    ps_sendCommandString(ps, enter_config, sizeof(enter_config)); //start config run

    //read type
    delayMicroseconds(CTRL_BYTE_DELAY);

    CMD_SET(ps);
    CLK_SET(ps);
    ATT_CLR(ps); // low enable joystick

    delayMicroseconds(CTRL_BYTE_DELAY);
    int i;
    for (i = 0; i<9; i++) {
      temp[i] = _ps_gamepad_shiftinout(ps, type_read[i]);
    }

    ATT_SET(ps); // HI disable joystick

    ps->controller_type = temp[3];

    ps_sendCommandString(ps, set_mode, sizeof(set_mode));
    if(rumble){ ps_sendCommandString(ps, enable_rumble, sizeof(enable_rumble)); ps->en_Rumble = true; }
    if(pressures){ ps_sendCommandString(ps, set_bytes_large, sizeof(set_bytes_large)); ps->en_Pressures = true; }
    ps_sendCommandString(ps, exit_config, sizeof(exit_config));

    ps_read_gamepad(ps);

    if(pressures){
      if(ps->PS2data[1] == 0x79)
        break;
      if(ps->PS2data[1] == 0x73)
        return 3;
    }

    if(ps->PS2data[1] == 0x73)
      break;

    if(y == 10){
#ifdef PS2X_DEBUG
      Serial.println("Controller not accepting commands");
      Serial.print("mode stil set at");
      Serial.println(PS2data[1], HEX);
#endif
      return 2; //exit function with error
    }
    ps->read_delay += 1; //add 1ms to read_delay
  }
  return 0; //no error if here
}

/****************************************************************************************/
void ps_sendCommandString(PS2X_t *ps, byte string[], byte len) {
#ifdef PS2X_COM_DEBUG
  byte temp[len];
  ATT_CLR(ps); // low enable joystick
  delayMicroseconds(CTRL_BYTE_DELAY);

  for (int y=0; y < len; y++)
    temp[y] = _gamepad_shiftinout(string[y]);

  ATT_SET(ps); //high disable joystick
  delay(read_delay); //wait a few

  Serial.println("OUT:IN Configure");
  for(int i=0; i<len; i++) {
    Serial.print(string[i], HEX);
    Serial.print(":");
    Serial.print(temp[i], HEX);
    Serial.print(" ");
  }
  Serial.println("");
#else
  ATT_CLR(ps); // low enable joystick
  int y;
  for ( y=0; y < len; y++)
    _ps_gamepad_shiftinout(ps, string[y]);
  ATT_SET(ps); //high disable joystick
  delay(ps->read_delay);                  //wait a few
#endif
}

/****************************************************************************************/
byte ps_readType(PS2X_t *ps) {
/*
  byte temp[sizeof(type_read)];

  sendCommandString(enter_config, sizeof(enter_config));

  delayMicroseconds(CTRL_BYTE_DELAY);

  CMD_SET(ps);
  CLK_SET(ps);
  ATT_CLR(ps); // low enable joystick

  delayMicroseconds(CTRL_BYTE_DELAY);

  for (int i = 0; i<9; i++) {
    temp[i] = _gamepad_shiftinout(type_read[i]);
  }

  sendCommandString(exit_config, sizeof(exit_config));

  if(temp[3] == 0x03)
    return 1;
  else if(temp[3] == 0x01)
    return 2;

  return 0;
*/

  if(ps->controller_type == 0x03)
    return 1;
  else if(ps->controller_type == 0x01)
    return 2;
  else if(ps->controller_type == 0x0C)
    return 3;  //2.4G Wireless Dual Shock PS2 Game Controller

  return 0;
}

/****************************************************************************************/
void ps_enableRumble(PS2X_t *ps) {
  ps_sendCommandString(ps, enter_config, sizeof(enter_config));
  ps_sendCommandString(ps, enable_rumble, sizeof(enable_rumble));
  ps_sendCommandString(ps, exit_config, sizeof(exit_config));
  ps->en_Rumble = true;
}

/****************************************************************************************/
boolean ps_enablePressures(PS2X_t *ps) {
  ps_sendCommandString(ps, enter_config, sizeof(enter_config));
  ps_sendCommandString(ps, set_bytes_large, sizeof(set_bytes_large));
  ps_sendCommandString(ps, exit_config, sizeof(exit_config));

  ps_read_gamepad(ps);
  ps_read_gamepad(ps);

  if(ps->PS2data[1] != 0x79)
    return false;

  ps->en_Pressures = true;
    return true;
}

/****************************************************************************************/
void ps_reconfig_gamepad(PS2X_t *ps){
  ps_sendCommandString(ps, enter_config, sizeof(enter_config));
  ps_sendCommandString(ps, set_mode, sizeof(set_mode));
  if (ps->en_Rumble)
    ps_sendCommandString(ps, enable_rumble, sizeof(enable_rumble));
  if (ps->en_Pressures)
    ps_sendCommandString(ps, set_bytes_large, sizeof(set_bytes_large));
  ps_sendCommandString(ps, exit_config, sizeof(exit_config));
}

/****************************************************************************************/
#ifdef __AVR__
inline void  CLK_SET(PS2X_t *ps) {
  register uint8_t old_sreg = SREG;
  cli();
  *ps->_clk_oreg |= ps->_clk_mask;
  SREG = old_sreg;
}

inline void  CLK_CLR(PS2X_t *ps) {
  register uint8_t old_sreg = SREG;
  cli();
  *ps->_clk_oreg &= ~ps->_clk_mask;
  SREG = old_sreg;
}

inline void  CMD_SET(PS2X_t *ps) {
  register uint8_t old_sreg = SREG;
  cli();
  *ps->_cmd_oreg |= ps->_cmd_mask; // SET(*_cmd_oreg,_cmd_mask);
  SREG = old_sreg;
}

inline void  CMD_CLR(PS2X_t *ps) {
  register uint8_t old_sreg = SREG;
  cli();
  *ps->_cmd_oreg &= ~ps->_cmd_mask; // SET(*_cmd_oreg,_cmd_mask);
  SREG = old_sreg;
}

inline void  ATT_SET(PS2X_t *ps) {
  register uint8_t old_sreg = SREG;
  cli();
  *ps->_att_oreg |= ps->_att_mask ;
  SREG = old_sreg;
}

inline void ATT_CLR(PS2X_t *ps) {
  register uint8_t old_sreg = SREG;
  cli();
  *ps->_att_oreg &= ~ps->_att_mask;
  SREG = old_sreg;
}

inline boolean DAT_CHK(PS2X_t *ps) {
  return (*ps->_dat_ireg & ps->_dat_mask) ? true : false;
}

#else
// On pic32, use the set/clr registers to make them atomic...
inline void CLK_SET(PS2X_t *ps) {
  *ps->_clk_lport_set |= ps->_clk_mask;
}

inline void CLK_CLR(PS2X_t *ps) {
  *ps->_clk_lport_clr |= ps->_clk_mask;
}

inline void CMD_SET(PS2X_t *ps) {
  *ps->_cmd_lport_set |= ps->_cmd_mask;
}

inline void CMD_CLR(PS2X_t *ps) {
  *ps->_cmd_lport_clr |= ps->_cmd_mask;
}

inline void  ATT_SET(PS2X_t *ps) {
  *ps->_att_lport_set |= ps->_att_mask;
}

inline void ATT_CLR(PS2X_t *ps) {
  *ps->_att_lport_clr |= ps->_att_mask;
}

inline boolean DAT_CHK(PS2X_t *ps) {
  return (*ps->_dat_lport & ps->_dat_mask) ? true : false;
}

#endif
