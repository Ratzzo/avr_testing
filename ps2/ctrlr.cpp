#include "PS2X_lib.h"  //for v1.6
#include "RF24.h"
#include "nRF24L01.h"
#include "printf.h"
#include "7seg.h"
#include "twi.h"
#include <avr/eeprom.h>
#include <Arduino.h>
#include <avr/io.h>
#include <math.h>
#include <SPI.h>
#include "common.h"

#define PS2_DAT        5
#define PS2_CMD        6
#define PS2_SEL        7
#define PS2_CLK        8

#define pressures   true
#define rumble      false

#define BOOTUP_PIN  9
#define LED1 A0
#define LED2 A1

PS2X ps2x;

RF24 rf(3,4);
seg7_t disp;

const uint64_t baseaddr = 0xF0F0F0F000LL;

int error = 0;
byte type = 0;
byte vibrate = 0;
uint32_t lastmillis = 0;
uint32_t startmillis = 0;
uint32_t ms = 0;
uint8_t initialized = 0; //once initialized we can use the indicator leds for other purpose such as RF connection
uint8_t led1_state = 0;
uint8_t led2_state = 0;


//eeprom map:
#define PIPE_INDEX_ADDR 0
uint8_t pipe_index = 0;
uint64_t device_address = 0;
uint64_t target_address = 0;


uint64_t getaddr(uint8_t index)
{
    uint64_t result = 0;
    uint8_t subaddr;
    eeprom_read_block(&subaddr, (void*)(PIPE_INDEX_ADDR + sizeof(pipe_index) + sizeof(uint8_t) + sizeof(uint8_t)*index), sizeof(uint8_t));
    result = baseaddr + subaddr;
    return result;
}

void setup()
{
    twi_init();
    pinMode(LED1, OUTPUT);
    pinMode(LED2, OUTPUT);
    Serial.begin(115200);
    delay(300);
    printf_begin();
    seg7_create(&disp, 0, 1);
    pipe_index = eeprom_read_byte(0);
    uint8_t subaddr = 0;
    eeprom_read_block((void*)&subaddr, (void*)(PIPE_INDEX_ADDR + sizeof(pipe_index)), sizeof(uint8_t));
    device_address = baseaddr + subaddr;
    seg7_setnumber(&disp, pipe_index);
    target_address = getaddr(disp.currentnumber);

    //initialize controller
    error = ps_config_gamepad2(&ps2x, PS2_CLK, PS2_CMD, PS2_SEL, PS2_DAT, pressures, rumble);
    type = ps_readType(&ps2x);
    if(type){
        digitalWrite(LED1, led1_state=1);
    }

    //initialize RF
    uint8_t rf_val = rf.begin();
    printf("rf.begin(); == %u", rf_val);
    if(!rf_val){
        rf.setRetries(15,15);
        rf.setPayloadSize(8);
        rf.openWritingPipe(target_address);
        rf.openReadingPipe(1,device_address);
        rf.startListening();
        rf.printDetails();
        digitalWrite(LED2, led1_state=1);
    }



    //confirm bootup with led
    DDRB |= _BV(DDB6);
    DDRB |= _BV(DDB7);
    pinMode(BOOTUP_PIN, OUTPUT);
    //PORTB |= _BV(PB6);
    lastmillis = millis();
    startmillis = lastmillis;
    ms = lastmillis;
}

int ledstate = 0;
float b = 0;

uint8_t last_LY = 127;
uint8_t last_LX = 127;
uint8_t last_RY = 127;
uint8_t last_RX = 127;
uint8_t analogchange = 0;

void loop()
{

    //turn of leds after 1000ms passed
    if(!initialized && millis() > startmillis + 500){
        digitalWrite(LED1, led1_state=0);
        digitalWrite(LED2, led2_state=0);
        initialized = 1;
    }


    if(millis() > ms + 10){
    double val;
	b += 0.01;
	val = 127 - 127*sin(b);
    analogWrite(BOOTUP_PIN, val);
    ms = millis();
    if(b >= PI) b = 0;
	}

    if(ledstate && millis() >= lastmillis + 10)
    {
        PORTB ^= _BV(PB6);
        ledstate = 0;
    }

    if(!initialized) //skip loop if not initialized
        return;



    ps_read_gamepad2(&ps2x, false, 0);
    if (ps_Newbuttonstate(&ps2x)) //handle button presses
    {

        //visually ack a button was pressed by blinking the light
        PORTB |= _BV(PB6);
        ledstate = 1;
        lastmillis = millis();

        //if select is being held

        if(ps_Button(&ps2x, PSB_SELECT)){
            digitalWrite(LED1, led1_state=1);
            if(ps_Button(&ps2x, PSB_PAD_UP)){
                seg7_setnumber(&disp, disp.currentnumber+1);
            } else if(ps_Button(&ps2x, PSB_PAD_DOWN)){
                seg7_setnumber(&disp, disp.currentnumber-1);
            } else if(ps_Button(&ps2x, PSB_PAD_LEFT)){
                seg7_setnumber(&disp, disp.currentnumber-10);
            } else if(ps_Button(&ps2x, PSB_PAD_RIGHT)){
                seg7_setnumber(&disp, disp.currentnumber+10);
            }
            rf.stopListening();
            eeprom_write_byte((uint8_t*)PIPE_INDEX_ADDR, disp.currentnumber);
            rf.openWritingPipe(getaddr(disp.currentnumber));
            rf.startListening();
            return;
        } else {
            digitalWrite(LED1, led1_state=0);
        }
        //if(ps2x.Button(PSB_START))         //will be TRUE as long as button is pressed

        //prepare buffer to send
        uint8_t packeddata[8];
        uint8_t *packeddataptr = packeddata;
        memset(packeddata, 0, sizeof(packeddata));

        //gather data and print to serial as binary
        uint16_t data = ps_ButtonDataByte(&ps2x);
        for(int i = 0; i < 16; i++)
            Serial.print((int)((data & (1 << i)) && 1));
        Serial.print("\n");

        //fill data in buffer, not using *ptr++ for readability.
        *packeddataptr = DATA_TYPE_BUTTON;
        packeddataptr += sizeof(DATA_TYPE_BUTTON);


        *((uint16_t*)packeddataptr) = data;
        packeddataptr += sizeof(data);




        //send data
        rf.stopListening();

        rf.write(packeddata, sizeof(packeddata));

        rf.startListening();
        return;
    }

    //determine if an analog change occurred
    analogchange = 0;
    uint8_t LX = 127, LY = 127, RX = 127, RY = 127;
    if(
       (LX = ps_Analog(&ps2x, PSS_LX)) != last_LX ||
       (LY = ps_Analog(&ps2x, PSS_LY)) != last_LY ||
       (RX = ps_Analog(&ps2x, PSS_RX)) != last_RX ||
       (RY = ps_Analog(&ps2x, PSS_RY)) != last_RY
    ){
        if(LX != 127)
            last_LX = LX;
        if(LY != 127)
            last_LY = LY;
        if(RX != 127)
            last_RX = RX;
        if(RY != 127)
            last_RY = RY;
        analogchange = 1;

        PORTB |= _BV(PB6);
        ledstate = 1;
        lastmillis = millis();
    }


    //process the analog change
    if(analogchange){

        //blink das lightchen
        PORTB |= _BV(PB6);
        lastmillis = millis();

        uint8_t packeddata[8];
        uint8_t *packeddataptr = packeddata;
        memset(packeddata, 0, sizeof(packeddata));

        //fill the buffer
        *packeddataptr = DATA_TYPE_STICK_ANALOG;
        packeddataptr += sizeof(uint8_t);


        *packeddataptr = last_LX;
        packeddataptr += sizeof(last_LX);

        *packeddataptr = last_LY;
        packeddataptr += sizeof(last_LY);

        *packeddataptr = last_RX;
        packeddataptr += sizeof(last_RX);

        *packeddataptr = last_RY;
        packeddataptr += sizeof(last_RY);

        rf.stopListening();

        rf.write(&packeddata, sizeof(packeddata));

        rf.startListening();
    }


//  delay(50);
}
