#include <Arduino.h>
#include <math.h>
#include <stdint.h>
#include <mcp23017.h>
#include <twi.h>
#include "dmui.h"
#include "keypad.h"

DisplayModuleUI *dp;

volatile int32_t backlight = 255;
volatile int32_t ledlight = 255;
volatile uint32_t led2 = 0;
volatile uint32_t step_interval = 20;
volatile uint32_t step_forward = 0;
unsigned long timer1 = 0;

dmui_prop_t props[] = {
DEFPROP("LED1", DMUI_PROP_UINT32, DMUI_READWRITE, ledlight, 0, 255),
DEFPROP("backlight", DMUI_PROP_UINT32, DMUI_READWRITE, backlight, 0, 255),
DEFPROP("step forward", DMUI_PROP_UINT32, DMUI_READWRITE, step_forward, 0, 1),
DEFPROP("step interval", DMUI_PROP_UINT32, DMUI_READWRITE, step_interval, 1, 60000),
DEFPROP("LED2", DMUI_PROP_UINT32, DMUI_READWRITE, led2, 0, 1),
DEFPROPEND
};

int stepper_pins[4] = {2,3,4,7}; 

typedef struct {
uint8_t p1 : 2;
uint8_t p2 : 2;
uint8_t p3 : 2;
uint8_t p4 : 2;
} step_t;

step_t stepmap[8] = {
{1, 0, 0, 0},
{1, 1, 0, 0},
{0, 1, 0, 0},
{0, 1, 1, 0},
{0, 0, 1, 0},
{0, 0, 1, 1},
{0, 0, 0, 1},
{1, 0, 0, 1}
};



void setup()  {
	twi_init(400000L);
	Serial.begin(115200);
	pinMode(9, INPUT);
	pinMode(5, OUTPUT);
	pinMode(6, OUTPUT);
	pinMode(13, OUTPUT);
	//stepper pins
	for(unsigned int i = 0; i < sizeof(stepper_pins)/sizeof(int); i++){
		pinMode(stepper_pins[i], OUTPUT);
		digitalWrite(stepper_pins[i], LOW);
	}
	dp = new DisplayModuleUI(9, 0, props);
	timer1 = millis();
	//dp->cb = keyhandler;
}

static int stepindex = 0;

void nextstep(){
	stepindex++;
	if(stepindex > 7) stepindex = 0;
	if(stepindex < 0) stepindex = 7;
	digitalWrite(stepper_pins[0], stepmap[stepindex].p1);
	digitalWrite(stepper_pins[1], stepmap[stepindex].p2);
	digitalWrite(stepper_pins[2], stepmap[stepindex].p3);
	digitalWrite(stepper_pins[3], stepmap[stepindex].p4);
}

void previousstep(){
	stepindex--;
	if(stepindex < 0) stepindex = 7;
	if(stepindex > 7) stepindex = 0;
	digitalWrite(stepper_pins[0], stepmap[stepindex].p1);
	digitalWrite(stepper_pins[1], stepmap[stepindex].p2);
	digitalWrite(stepper_pins[2], stepmap[stepindex].p3);
	digitalWrite(stepper_pins[3], stepmap[stepindex].p4);
}

void loop()  {
	dp->checkForEvent();
	analogWrite(5, ledlight);
	analogWrite(6, backlight);
	digitalWrite(13, led2);
	if(millis() > timer1 + step_interval){
		if(step_forward)
			nextstep();
			else
			previousstep();
		timer1 = millis();
	}
}
