#include "keypad.h"
#include <stdio.h>
#include <stdlib.h>



int mcp23017_keypad_create(mcp23017_keypad_t *kp, KeyPad_Port port, uint8_t addr)
{
	kp->p = port;
	mcp23017_create(&kp->mcp, addr);
	switch(port){
		case KP_PORTA:
		 //set 6,5,4, and 3 as inputs
			mcp23017_setbyte(&kp->mcp, MCP23017ADDR_IODIRA, 1 << 6 | 1 << 5 | 1 << 4 | 1 << 3);
			mcp23017_setbyte(&kp->mcp, MCP23017ADDR_GPIOA, 0x00); //set everything low
			//enable pullups on inputs
			mcp23017_setbyte(&kp->mcp, MCP23017ADDR_GPPUA, 1 << 6 | 1 << 5 | 1 << 4 | 1 << 3);
			
			
			mcp23017_setbyte(&kp->mcp, MCP23017ADDR_GPINTENA, 1 << 6 | 1 << 5 | 1 << 4 | 1 << 3);
			mcp23017_setbyte(&kp->mcp, MCP23017ADDR_DEFVALA, 1 << 6 | 1 << 5 | 1 << 4 | 1 << 3);
			mcp23017_setbyte(&kp->mcp, MCP23017ADDR_INTCONA, 1 << 6 | 1 << 5 | 1 << 4 | 1 << 3);
			//Serial.println(mcp23017_getbyte(&kp->mcp, MCP23017ADDR_GPIOA));
		break;
		case KP_PORTB:
			mcp23017_setbyte(&kp->mcp, MCP23017ADDR_IODIRB, 1 << 6 | 1 << 5 | 1 << 4 | 1 << 3);
			mcp23017_setbyte(&kp->mcp, MCP23017ADDR_GPIOB, 0x00); //set everything low
			//enable pullups on inputs
			mcp23017_setbyte(&kp->mcp, MCP23017ADDR_GPPUB, 1 << 6 | 1 << 5 | 1 << 4 | 1 << 3);
			
			
			mcp23017_setbyte(&kp->mcp, MCP23017ADDR_GPINTENB, 1 << 6 | 1 << 5 | 1 << 4 | 1 << 3);
			mcp23017_setbyte(&kp->mcp, MCP23017ADDR_DEFVALB, 1 << 6 | 1 << 5 | 1 << 4 | 1 << 3);
			mcp23017_setbyte(&kp->mcp, MCP23017ADDR_INTCONB, 1 << 6 | 1 << 5 | 1 << 4 | 1 << 3);
			//Serial.println(mcp23017_getbyte(&kp->mcp, MCP23017ADDR_GPIOA));
		break;
	}
	//mcp23017_keypad_checkForInput(kp);
	return 0;
}


uint16_t mcp23017_keypad_checkForInput(mcp23017_keypad_t *kp)
{
	uint8_t a = 0, b = 0, c = 0;
	switch(kp->p){
	case KP_PORTA:
		
		mcp23017_setbyte(&kp->mcp, MCP23017ADDR_GPIOA, (1 << 0));
		a = mcp23017_getbyte(&kp->mcp, MCP23017ADDR_GPIOA);
		mcp23017_setbyte(&kp->mcp, MCP23017ADDR_GPIOA, (1 << 1));
		b = mcp23017_getbyte(&kp->mcp, MCP23017ADDR_GPIOA);
		mcp23017_setbyte(&kp->mcp, MCP23017ADDR_GPIOA, (1 << 2));
		c = mcp23017_getbyte(&kp->mcp, MCP23017ADDR_GPIOA);
		
		
		mcp23017_setbyte(&kp->mcp, MCP23017ADDR_GPIOA, 0x00);
		
	break;
	case KP_PORTB:
		mcp23017_setbyte(&kp->mcp, MCP23017ADDR_GPIOB, (1 << 0));
		a = mcp23017_getbyte(&kp->mcp, MCP23017ADDR_GPIOB);
		mcp23017_setbyte(&kp->mcp, MCP23017ADDR_GPIOB, (1 << 1));
		b = mcp23017_getbyte(&kp->mcp, MCP23017ADDR_GPIOB);
		mcp23017_setbyte(&kp->mcp, MCP23017ADDR_GPIOB, (1 << 2));
		c = mcp23017_getbyte(&kp->mcp, MCP23017ADDR_GPIOB);
		
		
		mcp23017_setbyte(&kp->mcp, MCP23017ADDR_GPIOB, 0x00);
		
	break;
	}
	if(!((b | c) & (1 << 3))) return 1;
	if(!((a | c) & (1 << 3))) return 2;
	if(!((a | b) & (1 << 3))) return 3;
		
	if(!((b | c) & (1 << 4))) return 4;
	if(!((a | c) & (1 << 4))) return 5;
	if(!((a | b) & (1 << 4))) return 6;
		
	if(!((b | c) & (1 << 5))) return 7;
	if(!((a | c) & (1 << 5))) return 8;
	if(!((a | b) & (1 << 5))) return 9;
		
	if(!((b | c) & (1 << 6))) return 10;
	if(!((a | c) & (1 << 6))) return 11;
	if(!((a | b) & (1 << 6))) return 12;
	return 0;
}
