#ifndef DMUI_H
#define DMUI_H

#include "displaymodule.h"

#define DMUI_READWRITE 0
#define DMUI_READONLY 1


#define DEFPROP(caption, type, readonly, val, min, max) {sizeof(caption)-1, type, readonly, caption, (void*)&val, min, max}
#define DEFPROPEND {0, 0, 0, 0, 0}

enum dmui_modes {
	DMUI_MODE_NAVIGATE = 0,
	DMUI_MODE_EDIT,
	DMUI_MODE_END = DMUI_MODE_EDIT
};

enum dmui_property_types {
	DMUI_PROP_INT32 = 0,
	DMUI_PROP_UINT32
	/* warning: defining more types causes BLOAT */
};

//since we lack resources, we'll pack prop names using a size/mem pair
typedef struct {
	uint8_t size : 4;
	uint8_t type : 3;
	uint8_t readonly : 1;
	const char *name;
	void *val;
	uint32_t min;
	uint32_t max;
} dmui_prop_t;

class DisplayModuleUI: public DisplayModule{
public:
	char tempbuffer[16];
	uint8_t bufpos;
	uint8_t mode;
	int8_t propindex;
	uint8_t nprops;
	dmui_prop_t *props;
	//props is null terminated
	DisplayModuleUI(int ipin, int bkpin, dmui_prop_t *properties);

};

namespace dmui {
	void refresh(DisplayModuleUI *ui);
	void callbackprocessing(uint16_t key, void *origin);
	void writevalue(DisplayModuleUI *ui);
};


#endif