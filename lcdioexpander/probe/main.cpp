#include <stdio.h>

typedef unsigned char uchar;
int main()
{
uchar byte = 0;
uchar byte2 = 0;
//printf("0x%.2x\n", byte);
byte |= 1 << 0;
byte |= 1 << 2;
byte |= 1 << 4;
byte |= 1 << 6;

byte &= ~(1 << 6);
byte |= (!0 << 6);

int x;
for(x=0; x < 8; x++){
printf("%u", ((1 << x) & byte) == (1 << x));
}
printf("\n");
for(x=0; x < 8; x++){
printf("%u", ((1 << x) & byte2) == (1 << x));
}
printf("\n");
}
