#include "dmui.h"

char modenames[][DMUI_MODE_END+1] = {
"N",
"E"
};

DisplayModuleUI::DisplayModuleUI(int ipin, int bkpin, dmui_prop_t *properties): DisplayModule(ipin, bkpin)
{
	cb = dmui::callbackprocessing;
	mode = DMUI_MODE_NAVIGATE;
	propindex = 0;
	nprops = 0;
	props = properties;
	unsigned int n = 0;
	while(properties[n].size)
		n++;
	nprops = n;
	dmui::refresh(this);
	bufpos = 0;
}

#define issigned(prop) !(prop->type % 2)

void dmui::writevalue(DisplayModuleUI *ui){
	//does it exists?
	dmui_prop_t *prop = &ui->props[ui->propindex];
	if(!prop->val) return;
	/* TO DO: handle floats and text */
	ui->lcd->setCursor(0,1);
	
	if(ui->mode == DMUI_MODE_EDIT){
		ui->lcd->print(ui->tempbuffer);
		ui->lcd->cursor();
		ui->lcd->setCursor(ui->bufpos, 1);
	}
	else
	{
		ui->lcd->noCursor();
	if(issigned(prop))
		ui->lcd->print(*(int32_t*)prop->val);
		//ui->lcd->print("signed");
	else
		ui->lcd->print(*(uint32_t*)prop->val);
		//ui->lcd->print("unsigned");
	}

}

void dmui::refresh(DisplayModuleUI *ui){
	char buf[16];
	memset(buf, 0, sizeof(buf));
	memcpy(buf, ui->props[ui->propindex].name, ui->props[ui->propindex].size);
	ui->lcd->clear();
	ui->lcd->setCursor(15,1);
	ui->lcd->print(modenames[ui->mode]);
	ui->lcd->setCursor(0,0);
	ui->lcd->print(buf);
	writevalue(ui);
}

#define INCDEC 10
	
void dmui::callbackprocessing(uint16_t key, void *origin){
DisplayModuleUI *ui = (DisplayModuleUI*)origin;
dmui_prop_t *prop = &ui->props[ui->propindex];
int32_t v = 0;
	if(key == KP_KEY_MODE){
		
		ui->mode++;

		if(ui->mode > DMUI_MODE_END)	ui->mode = 0;
		if(ui->mode == DMUI_MODE_EDIT && prop->readonly) ui->mode = DMUI_MODE_NAVIGATE; 
		if(ui->mode == DMUI_MODE_EDIT){
		memset(ui->tempbuffer, 0, 16);
		ui->bufpos = 0;
		}
		if(ui->mode == DMUI_MODE_NAVIGATE && ui->bufpos && !prop->readonly){
			if(issigned(prop)){
				int32_t value;
				sscanf(ui->tempbuffer, "%ld", &value);
				if(value > (int32_t)prop->max) value = prop->max;
				if(value < (int32_t)prop->min) value = prop->min;
				*(int32_t*)prop->val = value;
			}
			else{
				uint32_t value;
				sscanf(ui->tempbuffer, "%lu", &value);
				if(value > prop->max) value = prop->max;
				if(value < prop->min) value = prop->min;
				*(uint32_t*)prop->val = value;
			}
		}
		//after changing the mode set it accordingly in the display
	}
	switch(ui->mode){
		case DMUI_MODE_NAVIGATE:
			switch(key){
				case 8: //up
				//increment value
				/* TO DO: handle floats and strings (this goes for up and down arrows) */
					if(prop->readonly) break;
					if(issigned(prop)){
						v = *(int32_t*)prop->val;
						if((v += INCDEC) > (int32_t)prop->max){
								*(int32_t*)prop->val = (int32_t)prop->max;
								break;
							}
						*(int32_t*)prop->val = v;
					}
					else {
						v = *(int32_t*)prop->val;
						if((v += INCDEC) > (int32_t)prop->max){ 
							*(uint32_t*)prop->val = (uint32_t)prop->max;
							break;
						}
						*(uint32_t*)prop->val = v;
					}
				break;
				case 2: //down
				//decrement value
					if(prop->readonly) break;
					if(issigned(prop)){
						v = *(int32_t*)prop->val;
						if((v -= INCDEC) < (int32_t)prop->min){
								*(int32_t*)prop->val = (int32_t)prop->min;
								break;
							}
						*(int32_t*)prop->val = v;
					}
					else {
						v = *(int32_t*)prop->val;
						if((v -= INCDEC) < (int32_t)prop->min){ 
							*(uint32_t*)prop->val = (uint32_t)prop->min;
							break;
						}
						*(uint32_t*)prop->val = v;
					}
				break;
				case 6: //right
				//next element
					if(++ui->propindex >= ui->nprops)
						ui->propindex = 0;
				
				break;
				case 4: //left
				//previous element
					if(--ui->propindex < 0)
						ui->propindex = ui->nprops - 1;
				break;
				case 5: //enter
				break;
			}
		break;
		case DMUI_MODE_EDIT:
			if(key < 10 || key == KP_KEY_FUNC){
				if(ui->bufpos < 11){
					char temp[3];
					if(key == KP_KEY_FUNC)
					{				
						if(!issigned(prop)) break;
						temp[0] = '-';
						temp[1] = 0;
					}
					else
						sprintf(temp, "%u", key);
					memcpy(ui->tempbuffer + ui->bufpos, temp, 1);
					ui->bufpos++;
				}
			}
		break;
	}
	refresh(ui);
}