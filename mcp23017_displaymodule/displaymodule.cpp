#include "displaymodule.h"

uint8_t subs[] = {
255,
KP_KEY_FUNC,
0,
KP_KEY_MODE,
3,
2,
1,
6,
5,
4,
9,
8,
7
};//substitution array

DisplayModule::DisplayModule(uint16_t interruptpin /* input */, uint16_t backlightpin /* output */)
{
	delay(100);
//	Serial.println("DisplayModule::DisplayModule");
	cb = 0;
	refreshms = millis();
	mcp23017_create(&mcp, 0);
	mcp23017_setbyte(&mcp, MCP23017ADDR_IODIRB, 0x00);
	mcp23017_setbyte(&mcp, MCP23017ADDR_GPIOB, 0x00 | 1 << 7);
	ipin = interruptpin;
	blpin = backlightpin;
	lastl = 0;
	keylock = 0;
	//create keypad
	mcp23017_keypad_create(&keypad, KP_PORTA, 0);
	lcd = new MCPLiquidCrystal(&mcp, MCP23017ADDR_GPIOB, 0, 1, 2, 3, 4, 5, 6);
  lcd->begin(16, 2);
}

inline void DisplayModule::setBackLight(double bl)
{

}

uint16_t DisplayModule::checkForEvent(){
	if(!cb) return 0;
	int rv = 0;
	if(!digitalRead(ipin)){
		keylock = 1;
		refreshms = millis();
		uint16_t i = mcp23017_keypad_checkForInput(&keypad);
		if(i != lastl && i){
			cb(subs[i], (void*)this);
//			Serial.println(lastl);
			keylock = 0;
		}
		lastl = i;
	}
	if(millis() > refreshms + 10 && keylock){
		keylock = 0;
		lastl = 0;
	}
	return rv;
}

