#ifndef KEYPAD_H
#define KEYPAD_H

#include <stdint.h>
#include <mcp23017.h>
#include <Arduino.h>

enum KeyPad_Port {
	KP_PORTA = 0,
	KP_PORTB
};

typedef struct {
	volatile KeyPad_Port p;
    mcp23017_t mcp;
} mcp23017_keypad_t;

	int mcp23017_keypad_create(mcp23017_keypad_t *kp, KeyPad_Port port, uint8_t iaddr);
	uint16_t mcp23017_keypad_checkForInput(mcp23017_keypad_t *kp);
	

#endif