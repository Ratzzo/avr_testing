#include <Arduino.h>
#include <math.h>
#include <stdint.h>
#include <mcp23017.h>
#include <twi.h>
#include "dmui.h"
#include "keypad.h"

DisplayModuleUI *dp;

volatile int32_t backlight = 255;
volatile int32_t ledlight = 255;
volatile uint32_t led2 = 0;

dmui_prop_t props[] = {
DEFPROP("LED1", DMUI_PROP_UINT32, DMUI_READWRITE, ledlight, 0, 255),
DEFPROP("backlight", DMUI_PROP_UINT32, DMUI_READWRITE, backlight, 0, 255),
DEFPROP("LED2", DMUI_PROP_INT32, DMUI_READWRITE, led2, 0, 1),
DEFPROPEND
};

void setup()  {
	twi_init(400000L);
  //digitalWrite(SDA, 1);
  //digitalWrite(SCL, 1);
	Serial.begin(115200);
	pinMode(8, INPUT);
	pinMode(5, OUTPUT);
	pinMode(6, OUTPUT);
	pinMode(13, OUTPUT);
	pinMode(7, OUTPUT);
	dp = new DisplayModuleUI(8, 0, props);
	digitalWrite(7, LOW);
	//dp->cb = keyhandler;
}

void loop()  {
	dp->checkForEvent();
	analogWrite(5, ledlight);
	analogWrite(6, backlight);
	digitalWrite(13, led2);
}
