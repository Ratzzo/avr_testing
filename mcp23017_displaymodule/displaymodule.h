#ifndef DISPLAYMODULE_H
#define DISPLAYMODULE_H

#include <stdint.h>
#include <mcp23017.h>
#include <lcd.h>
#include "keypad.h"
#include <Arduino.h>

#define KP_KEY_FUNC 20
#define KP_KEY_MODE  30

typedef void (*kp_callback_t)(uint16_t key, void *origin);

class DisplayModule
{
protected:
	uint8_t blpin, ipin;
	bool keylock;
	uint16_t lastl;
	unsigned long refreshms;
	mcp23017_t mcp;
	mcp23017_keypad_t keypad;
public:
	MCPLiquidCrystal *lcd;
	kp_callback_t cb;
	//note: the interruptpin is not a real interrupt
	DisplayModule(uint16_t interruptpin /* input */, uint16_t backlightpin /* output */);
	uint16_t checkForEvent();
	inline void setBackLight(double bl);
};

#endif
