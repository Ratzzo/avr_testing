#include "mcp23017.h"
#include <twi.h>

/*
uchar mcp23017_refresh(mcp23017_t *mcps){ //update registers
uchar bytestosend[2];
bytestosend[0] = MCP23017ADDR_IODIRA;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->IODIRA, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_IODIRB;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->IODIRB, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_IPOLA;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->IPOLA, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_IPOLB;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->IPOLB, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_GPINTENA;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->GPINTENA, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_GPINTENB;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->GPINTENB, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_DEFVALA;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->DEFVALA, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_DEFVALB;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->DEFVALB, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_INTCONA;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->INTCONA, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_INTCONB;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->INTCONB, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_IOCONA;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->IOCONA, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_IOCONB;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->IOCONB, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_GPPUA;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->GPPUA, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_GPPUB;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->GPPUB, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_INTFA;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->INTFA, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_INTFB;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->INTFB, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_INTCAPA;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->INTCAPA, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_INTCAPB;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->INTCAPB, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_GPIOA;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->GPIOA, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_GPIOB;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->GPIOB, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_OLATA;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->OLATA, 1, 1); //receive and stop

bytestosend[0] = MCP23017ADDR_OLATB;//register addr
twi_writeTo(mcps->raddress, bytestosend, 1, 0, 1); //send and stop
twi_readFrom(mcps->raddress, &mcps->OLATB, 1, 1); //receive and stop

return 0;
}

uchar mcp23017_update(mcp23017_t *mcps){ //upload registers in memory to
uchar bytestosend[2];
bytestosend[0] = MCP23017ADDR_IODIRA;//register addr
bytestosend[1] = mcps->IODIRA;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

bytestosend[0] = MCP23017ADDR_IODIRB;//register addr
bytestosend[1] = mcps->IODIRB;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

bytestosend[0] = MCP23017ADDR_IPOLA;//register addr
bytestosend[1] = mcps->IPOLA;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

bytestosend[0] = MCP23017ADDR_IPOLB;//register addr
bytestosend[1] = mcps->IPOLB;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

bytestosend[0] = MCP23017ADDR_GPINTENA;//register addr
bytestosend[1] = mcps->GPINTENA;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

bytestosend[0] = MCP23017ADDR_GPINTENB;//register addr
bytestosend[1] = mcps->GPINTENB;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

bytestosend[0] = MCP23017ADDR_DEFVALA;//register addr
bytestosend[1] = mcps->DEFVALA;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

bytestosend[0] = MCP23017ADDR_DEFVALB;//register addr
bytestosend[1] = mcps->DEFVALB;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

bytestosend[0] = MCP23017ADDR_INTCONA;//register addr
bytestosend[1] = mcps->INTCONA;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

bytestosend[0] = MCP23017ADDR_INTCONB;//register addr
bytestosend[1] = mcps->INTCONB;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

bytestosend[0] = MCP23017ADDR_IOCONA;//register addr
bytestosend[1] = mcps->IOCONA;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

bytestosend[0] = MCP23017ADDR_IOCONB;//register addr
bytestosend[1] = mcps->IOCONB;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

bytestosend[0] = MCP23017ADDR_GPPUA;//register addr
bytestosend[1] = mcps->GPPUA;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

bytestosend[0] = MCP23017ADDR_GPPUB;//register addr
bytestosend[1] = mcps->GPPUB;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

bytestosend[0] = MCP23017ADDR_GPIOA;//register addr
bytestosend[1] = mcps->GPIOA;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

bytestosend[0] = MCP23017ADDR_GPIOB;//register addr
bytestosend[1] = mcps->GPIOB;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

bytestosend[0] = MCP23017ADDR_OLATA;//register addr
bytestosend[1] = mcps->OLATA;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

bytestosend[0] = MCP23017ADDR_OLATB;//register addr
bytestosend[1] = mcps->OLATB;
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

return 0;
}
*/

uchar mcp23017_create(mcp23017_t *mcps, uchar addr){
uchar bytestosend[2];

mcps->address = addr;
mcps->raddress = MCP23017_FIXEDADDR | addr;
//mcp23017_refresh(mcps);
return 0;
}

uchar mcp23017_setbyte(mcp23017_t *mcps, uchar regaddr, uchar data){
uchar bytestosend[2];
bytestosend[0] = regaddr;//register addr
bytestosend[1] = data;//byte to set
twi_writeTo(mcps->raddress, bytestosend, 2, 0, 1); //send and stop

return 0;
}

uchar mcp23017_getbyte(mcp23017_t *mcps, uchar regaddr){
uchar bytes[2];
bytes[0] = regaddr;//register addr
twi_writeTo(mcps->raddress, bytes, 1, 0, 1); //send and reset
twi_readFrom(mcps->raddress, bytes+1, 1, 1);
return bytes[1];
}



