#ifndef MCP23017_H
#define MCP23017_H

/*
Address
IOCON.BANK = 1
Address
IOCON.BANK = 0
Access to:
00h 00h IODIRA
10h 01h IODIRB
01h 02h IPOLA
11h 03h IPOLB
02h 04h GPINTENA
12h 05h GPINTENB
03h 06h DEFVALA
13h 07h DEFVALB
04h 08h INTCONA
14h 09h INTCONB
05h 0Ah IOCON //a
15h 0Bh IOCON //b
06h 0Ch GPPUA
16h 0Dh GPPUB
07h 0Eh INTFA
17h 0Fh INTFB
08h 10h INTCAPA
18h 11h INTCAPB
09h 12h GPIOA
19h 13h GPIOB
0Ah 14h OLATA
1Ah 15h OLATB
*/

/*
s    = start
p    = stop
sr   = restart
op   = device 7bit addr = 0x20 | [AD2][AD1][AD0]
w    = write bit        = 0
r    = read bit         = 1
addr = register address = 0x0 -> 0x15
din  = data to the device
dout = data from the device

I2C write
[s][op][w][addr][din]..[din][p]

I2C read last addr
[s][op][r][dout]..[dout][p]

I2C read specific register
[s][op][w][addr][sr][op][r][dout]..[dout][p]
*/

//registers are separated in a and b with two 8 bit banks
//[bit7][bit6][bit5][bit4][bit3][bit2][bit1][bit0]

#define MCP23017ADDR_IODIRA   0x0
#define MCP23017ADDR_IODIRB   0x1
/* IODIR � I/O DIRECTION REGISTER (ADDR 0x00)
1 = Pin is configured as an input.
0 = Pin is configured as an output.
*/

#define MCP23017ADDR_IPOLA    0x2
#define MCP23017ADDR_IPOLB    0x3
/* IPOL � INPUT POLARITY PORT REGISTER (ADDR 0x01)
1 = GPIO register bit will reflect the opposite logic state of the input pin.
0 = GPIO register bit will reflect the same logic state of the input pin.
*/

#define MCP23017ADDR_GPINTENA 0x4
#define MCP23017ADDR_GPINTENB 0x5
/* GPINTEN � INTERRUPT-ON-CHANGE PINS (ADDR 0x02)
The GPINTEN register controls the interrupt-onchange feature for each pin.
If a bit is set, the corresponding pin is enabled for
interrupt-on-change. The DEFVAL and INTCON
registers must also be configured if any pins are
enabled for interrupt-on-change.

1 = Enable GPIO input pin for interrupt-on-change event.
0 = Disable GPIO input pin for interrupt-on-change event.
*/

#define MCP23017ADDR_DEFVALA  0x6
#define MCP23017ADDR_DEFVALB  0x7
/* DEFVAL � DEFAULT VALUE REGISTER (ADDR 0x03)
The default comparison value is configured in the
DEFVAL register. If enabled (via GPINTEN and
INTCON) to compare against the DEFVAL register, an
opposite value on the associated pin will cause an
interrupt to occur.

If the associated pin level is the opposite from the register bit, an interrupt occurs.
*/

#define MCP23017ADDR_INTCONA  0x8
#define MCP23017ADDR_INTCONB  0x9
/* INTCON � INTERRUPT-ON-CHANGE CONTROL REGISTER (ADDR 0x04)
The INTCON register controls how the associated pin
value is compared for the interrupt-on-change feature.
If a bit is set, the corresponding I/O pin is compared
against the associated bit in the DEFVAL register. If a
bit value is clear, the corresponding I/O pin is compared
against the previous value.

1 = Controls how the associated pin value is compared for interrupt-on-change.
0 = Pin value is compared against the previous pin value
*/

#define MCP23017ADDR_IOCONA   0xA
#define MCP23017ADDR_IOCONB   0xB
/* CONFIGURATION REGISTER
[BANK][MIRROR][SEQOP][DISSLW][HAEN][ODR][INTPOL][Unimplemented]
- BANK: Controls how the registers are addressed
1 = The registers associated with each port are separated into different banks
0 = The registers are in the same bank (addresses are sequential)
- MIRROR: INT Pins Mirror bit
1 = The INT pins are internally connected
0 = The INT pins are not connected. INTA is associated with PortA and INTB is associated with PortB
- SEQOP: Sequential Operation mode bit.
1 = Sequential operation disabled, address pointer does not increment.
0 = Sequential operation enabled, address pointer increments.
- DISSLW: Slew Rate control bit for SDA output.
1 = Slew rate disabled.
0 = Slew rate enabled.
- HAEN: Hardware Address Enable bit (MCP23S17 only).
Address pins are always enabled on MCP23017.
1 = Enables the MCP23S17 address pins.
0 = Disables the MCP23S17 address pins.
- ODR: This bit configures the INT pin as an open-drain output.
1 = Open-drain output (overrides the INTPOL bit).
0 = Active driver output (INTPOL bit sets the polarity).
- INTPOL: This bit sets the polarity of the INT output pin.
1 = Active-high.
0 = Active-low.
- Unimplemented: Read as �0�
*/

#define MCP23017ADDR_GPPUA    0xC
#define MCP23017ADDR_GPPUB    0xD
/* PULL-UP RESISTOR CONFIGURATION REGISTER
The GPPU register controls the pull-up resistors for the
port pins. If a bit is set and the corresponding pin is
configured as an input, the corresponding port pin is
internally pulled up with a 100 kOhm resistor.

1 = Pull-up enabled.
0 = Pull-up disabled.
*/

#define MCP23017ADDR_INTFA    0xE
#define MCP23017ADDR_INTFB    0xF
/* INTERRUPT FLAG REGISTER
The INTF register reflects the interrupt condition on the
port pins of any pin that is enabled for interrupts via the
GPINTEN register. A �set� bit indicates that the
associated pin caused the interrupt.
This register is �read-only�. Writes to this register will be
ignored.

1 = Pin caused interrupt.
0 = Interrupt not pending.
*/

#define MCP23017ADDR_INTCAPA  0x10
#define MCP23017ADDR_INTCAPB  0x11
/* INTERRUPT CAPTURE REGISTER
The INTCAP register captures the GPIO port value at
the time the interrupt occurred. The register is �read
only� and is updated only when an interrupt occurs. The
register will remain unchanged until the interrupt is
cleared via a read of INTCAP or GPIO.

1 = Logic-high.
0 = Logic-low.
*/

#define MCP23017ADDR_GPIOA    0x12
#define MCP23017ADDR_GPIOB    0x13
/* PORT REGISTER
The GPIO register reflects the value on the port.
Reading from this register reads the port. Writing to this
register modifies the Output Latch (OLAT) register.

1 = Logic-high.
0 = Logic-low.
*/

#define MCP23017ADDR_OLATA    0x14
#define MCP23017ADDR_OLATB    0x15
/* OUTPUT LATCH REGISTER
The OLAT register provides access to the output
latches. A read from this register results in a read of the
OLAT and not the port itself. A write to this register
modifies the output latches that modifies the pins
configured as outputs.

1 = Logic-high.
0 = Logic-low.
*/

#define MCP23017_FIXEDADDR    0x20

typedef unsigned char uchar;
typedef struct mcp23017_t {
//uchar IODIRA, IODIRB;
//uchar IPOLA, IPOLB;
//uchar GPINTENA, GPINTENB;
//uchar DEFVALA, DEFVALB;
//uchar INTCONA, INTCONB;
//uchar IOCONA, IOCONB;
//uchar GPPUA, GPPUB;
//uchar INTFA, INTFB;
//uchar INTCAPA, INTCAPB;
//uchar GPIOA, GPIOB;
//uchar OLATA, OLATB;
volatile uchar address; //3 bit I2C addr. This is "OR"ed with 0x20
volatile uchar raddress; //7 bit I2C addr.
} mcp23017_t;

//uchar mcp23017_refresh(mcp23017_t *mcps); //get registers
//uchar mcp23017_update(mcp23017_t *mcps); //upload registers
uchar mcp23017_create(mcp23017_t *mcps, uchar addr);
uchar mcp23017_setbyte(mcp23017_t *mcps, uchar regaddr, uchar data);
uchar mcp23017_getbyte(mcp23017_t *mcps, uchar regaddr);

#endif
