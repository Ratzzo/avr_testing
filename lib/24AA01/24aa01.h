#ifndef T24AA01_H
#define T24AA01_H

/*
the 24aa01 is organized in pages and has 8 pages of 16 bytes (16x8) formerly 1kbit eeprom.
each page is located addressing a multiple of 0x10 (16 dec), so page addresses thus are:
0x00
0x10
0x20
0x30
0x40
0x50
0x60
0x70
containing 16 bytes each.
if you specify 0x03 for example then the data will wrap around the page, and the three last bytes would
be the ones corresponding to addresses 0x00, 0x01, and 0x02 respectively.
*/

extern void aa01_write_byte(byte deviceaddress, int eeaddress, byte data);
extern void aa01_write_page(byte deviceaddress, unsigned eeaddr, const byte *data, byte length);
extern int aa01_read_byte(byte deviceaddress, unsigned eeaddr);
extern int aa01_read_buffer(byte deviceaddr, unsigned eeaddr, byte * buffer, byte length);
					  

#endif