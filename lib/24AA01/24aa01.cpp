/* 
  *  Use the I2C bus with small EEPROMs
  *  24C01, 20C02, 24C04, 24C08, 24C16
  *  Sketch:    I2C_EEPROM_Small.pde
  *  
  *  Derived from sketch for 24C64 devices posted on
  *     http://www.arduino.cc/playground/Code/I2CEEPROM
  *  From  hkhijhe   Date: 01/10/2010
  * 
  *  This one by davekw7x
  *  March, 2011
  *
  * For a single device, connect as follows:
  * EEPROM 4 (GND) to GND
  * EEPROM 8 (Vcc) to Vcc (5 Volts)
  * EEPROM 5 (SDA) to Arduino Analog Pin 4
  * EEPROM 6 (SCL) to Arduino Analog Pin 5
  * EEPROM 7 (WP)  to GND
  * EEPROM 1 (A0)  to GND
  * EEPROM 2 (A1)  to GND
  * EEPROM 3 (A2)  to GND
  */
#include <Arduino.h>
#include <stdio.h>
#include <Wire.h>

void aa01_write_byte(byte deviceaddress, int eeaddress, byte data)
{
    // Three lsb of Device address byte are bits 8-10 of eeaddress
    byte devaddr = deviceaddress | ((eeaddress >> 8) & 0x07);
    byte addr    = eeaddress;
    Wire.beginTransmission(devaddr);
    Wire.write(int(addr));
    Wire.write(int(data));
    Wire.endTransmission();
    delay(10);
}

  // Pages are blocks of 16 bytes, starting at 0x000.
  // That is, pages start at 0x000, 0x010, 0x020, ...
  // For a device "page write", the last byte must be
  // on the same page as the first byte.
  //
  // No checking is done in this routine.
  //
  // TODO: Do some checking, or, better yet (maybe)
  // make length an int and do repeated device
  // page writes if necessary. (Then maybe rename to
  // eeprom_write_pages or some such thing.)
  //
void aa01_write_page(byte deviceaddress, unsigned eeaddr,
                       const byte * data, byte length)
{
    // Three lsb of Device address byte are bits 8-10 of eeaddress
    byte devaddr = deviceaddress | ((eeaddr >> 8) & 0x07);
    byte addr    = eeaddr;
    Wire.beginTransmission(devaddr);
    Wire.write(int(addr));
    for (int i = 0; i < length; i++) {
        Wire.write(data[i]);
    }
    Wire.endTransmission();
    delay(10);
}

// TODO: Change to integer data type and return -1 if can't
// read.
//
int aa01_read_byte(byte deviceaddress, unsigned eeaddr)
{
    byte rdata = -1;

    // Three lsb of Device address byte are bits 8-10 of eeaddress
    byte devaddr = deviceaddress | ((eeaddr >> 8) & 0x07);
    byte addr    = eeaddr;

    Wire.beginTransmission(devaddr);
    Wire.write(int(addr));
    Wire.endTransmission();
    Wire.requestFrom(int(devaddr), 1);
    if (Wire.available()) {
        rdata = Wire.read();
    }
    return rdata;
}

//
// Returns number of bytes read from device
//
// Due to buffer size in the Wire library, don't read more than 30 bytes
// at a time!  No checking is done in this function.
//
// TODO: Change length to int and make it so that it does repeated
// EEPROM reads for length greater than 30.

int aa01_read_buffer(byte deviceaddr, unsigned eeaddr,
                        byte * buffer, byte length)
{
    // Three lsb of Device address byte are bits 8-10 of eeaddress
    byte devaddr = deviceaddr | ((eeaddr >> 8) & 0x07);
    byte addr    = eeaddr;
    
    Wire.beginTransmission(devaddr);
    Wire.write(int(addr));
    Wire.endTransmission();

    Wire.requestFrom(devaddr, length);
    int i;
    for (i = 0; i < length && Wire.available(); i++) {
        buffer[i] = Wire.read();
    }
    return i;
}