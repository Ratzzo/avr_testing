#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/blowfish.h>
/*
blowfish is a symmetric block cipher
and uses a key of 448 bits (28 bytes)
and blocks of 64 bits(8bytes)
*/

static const unsigned char key[28] = {
0,0,0,0,
0,0,0,0,
0,0,0,0,
0,0,0,0,
0,0,0,0,
0,0,0,0,
0,0,0,0};

int main()
{

unsigned char inout[] = "\0\0\0\0\0\0\0\0";

BF_KEY blowfish_key = {0};

BF_set_key(&blowfish_key, 28, key);

//printf("%u",  ERR_get_error());

int i = 0;
for(i=0;i<8;i++)
printf("%2X ", inout[i]);
printf("\n\n");

BF_encrypt((unsigned int*)inout, &blowfish_key);

for(i=0;i<8;i++)
printf("%2X ", inout[i]);
printf("\n\n");

BF_decrypt((unsigned int*)inout, &blowfish_key);

for(i=0;i<8;i++)
printf("%2X ", inout[i]);
printf("\n\n");

    return 0;
}
