#include <Arduino.h>
#include <twi.h>
//#include <SPI.h>
#include <MCP23017.h>
#include <avr/io.h>
#include <stdarg.h>

Tmcp23017 mcp0;
Tmcp23017 mcp1;

void buttonproc();
void updatedisplay();

void p(char *fmt, ... ){
        char tmp[128]; // resulting string limited to 128 chars
        va_list args;
        va_start (args, fmt );
        vsnprintf(tmp, 128, fmt, args);
        va_end (args);
        Serial.print(tmp);
}

void setup()  {
DDRB |= (1<<DDB5);
PORTB |= (1<<PORTB5);
twi_init(400000L);
Serial.begin(9600);
DDRD |= (0<<DDD2);
PORTD |= (0<<PORTD2);
attachInterrupt(0, buttonproc, LOW);
mcp23017_create(&mcp0, 0);
mcp23017_create(&mcp1, 1);
//delay();

}

typedef uchar fourbytes[4];


long int del = 10;

//#pragma pack(1)

typedef uchar frame[6][7];

#define none 0x1D

frame test = {
{0,1,1,0,1,1,0},
{1,1,1,1,1,1,1},
{1,1,1,1,1,1,1},
{0,1,1,1,1,1,0},
{0,0,1,1,1,0,0},
{0,0,0,1,0,0,0}
};

frame test2 = {
{0,0,0,0,0,0,0},
{0,1,1,0,1,1,0},
{0,1,1,1,1,1,0},
{0,0,1,1,1,0,0},
{0,0,0,1,0,0,0},
{0,0,0,0,0,0,0}
};

frame shiftmap = {
{none, 0x0a, 0x0e, none, 0x17, 0x12, none},
{0x08, 0x0b, 0x0f, 0x04, 0x16, 0x11, 0x19},
{0x09, 0x0c, 0x07, 0x03, 0x15, 0x10, 0x1a},
{none, 0x0d, 0x06, 0x02, 0x14, 0x18, none},
{none, none, 0x05, 0x01, 0x13, none, none},
{none, none, none, 0x00, none, none, none}
};
/*
#define asd 3
frame shiftmap = {
{asd, asd, asd, asd, asd, asd, asd},
{asd, asd, asd, asd, asd, asd, asd},
{asd, asd, asd, asd, asd, asd, asd},
{asd, asd, asd, asd, asd, asd, asd},
{asd, asd, asd, asd, asd, asd, asd},
{asd, asd, asd, asd, asd, asd, asd}
};
*/
unsigned int a; 

void printbits(uchar i){
int n = 0;
for(;n<8;n++)
p("%u", (i & (1 << (7-n))) == (1 << (7-n)));
p("\n");
}

void lshiftproc(fourbytes inout, uchar places){
uchar temp2;
uchar temp;
//places = 26;
signed char oldplaces = places;
uchar p7 = places/7;
uchar l;

//p("%u\n\n\n", places);

/*for(l=0;l<=p7;l++)*/while(oldplaces != 0){

if(oldplaces - 7 >= 0)
{
oldplaces -= 7;
places = 7;
}
else
{
places = oldplaces;
oldplaces = 0;
}


temp = 0xff;
temp2 =0xff;

temp <<= places;
temp = ~(temp >> places);
temp &= inout[0];
temp >>= 8-places;
inout[0] <<= places;

temp2 <<= places;
temp2 = ~(temp2 >> places);
temp2 &= inout[1];
temp2 >>= 8-places;
inout[1] <<= places;
inout[1] |= temp;

temp = 0xff;
temp <<= places;
temp = ~(temp >> places);
temp &= inout[2];
temp >>= 8-places;
inout[2] <<= places;
inout[2] |= temp2;

inout[3] <<= places;
inout[3] |= temp;

}
//printbits(inout[0]);
//printbits(inout[1]);
//printbits(inout[2]);
//printbits(inout[3]);

}


void andproc(fourbytes inout, fourbytes by){
inout[0] = inout[0] & by[0];
inout[1] = inout[1] & by[1];
inout[2] = inout[2] & by[2];
inout[3] = inout[3] & by[3];
}
void orproc(fourbytes inout, fourbytes by){
inout[0] = inout[0] | by[0];
inout[1] = inout[1] | by[1];
inout[2] = inout[2] | by[2];
inout[3] = inout[3] | by[3];
}

void setproc(fourbytes inout, fourbytes by){
inout[0] = by[0];
inout[1] = by[1];
inout[2] = by[2];
inout[3] = by[3];
}

//uchar memcmp

fourbytes x = {0,0,0,0};
fourbytes temp = {0,0,0,0};
fourbytes onesource = {1,0,0,0};
fourbytes zero = {0,0,0,0};
fourbytes full = {0xff,0xff,0xff,0xff};



void setframe(frame in){
uchar m;
uchar n;

setproc(x, zero);

for(m=0;m<6;m++)
for(n=0;n<7;n++)
{
setproc(temp, onesource);
if(in[m][n] != 0){
lshiftproc(temp, shiftmap[m][n]);
orproc(x, temp);
}
//updatedisplay();
/*printbits(temp[0]);
printbits(temp[1]);
printbits(temp[2]);
printbits(temp[3]);
p("\n\n\n");
*/
//delay(1000);
//p("%2x, %2x, %2x, %2x\n", x[1], x[2], x[3], x[4]);
}
}

void buttonproc(){
static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > 50) 
  {
  setframe(test);
/*  setproc(temp, x);  
  lshiftproc(x, 1);
  orproc(x, temp);
  if(memcmp(x, full, sizeof(fourbytes)) == 0){
  x[0] = 0x1;
  x[1] = 0;
  x[2] = 0;
  x[3] = 0;
}*/
//	p("%2x, %2x, %2x, %2x\n", x[0], x[1], x[2], x[3]);
 
	
  }
  last_interrupt_time = interrupt_time;

}

void updatedisplay(){
mcp23017_setbyte(&mcp0, MCP23017ADDR_IODIRA, 0x00);
mcp23017_setbyte(&mcp0, MCP23017ADDR_IODIRB, 0x00);
mcp23017_setbyte(&mcp1, MCP23017ADDR_IODIRA, 0x00);
mcp23017_setbyte(&mcp1, MCP23017ADDR_IODIRB, 0x00);

mcp23017_setbyte(&mcp0, MCP23017ADDR_GPIOA, x[0]);
mcp23017_setbyte(&mcp0, MCP23017ADDR_GPIOB, x[1]);
mcp23017_setbyte(&mcp1, MCP23017ADDR_GPIOA, x[2]);
mcp23017_setbyte(&mcp1, MCP23017ADDR_GPIOB, x[3]);
}

void loop()  {
setframe(test);
updatedisplay();
delay(500);
setframe(test2);
updatedisplay();
delay(500);
PORTB ^= (1<<PORTB5);
}
