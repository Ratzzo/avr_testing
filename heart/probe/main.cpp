//#include <Arduino.h>
//#include <twi.h>
//#include <SPI.h>
//#include <MCP23017.h>
//#include <avr/io.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
//Tmcp23017 mcp0;
//Tmcp23017 mcp1;

void buttonproc();
#define p(...) printf(__VA_ARGS__)
//void p(char *fmt, ... ){
//        Serial.print(tmp);
//}

typedef unsigned char uchar;

typedef uchar fourbytes[4];


long int del = 10;

//#pragma pack(1)

typedef uchar frame[6][7];

const uchar none = 0x1D;

frame test = {
{0,0,0,0,0,0,0},
{0,0,0,0,0,0,0},
{0,0,0,0,0,0,0},
{0,0,0,0,0,0,0},
{0,0,0,0,0,0,0},
{0,0,0,0,0,0,0}
};


frame shiftmap = {
{none, 0x0a, 0x0e, none, 0x17, 0x12, none},
{0x08, 0x0b, 0x0f, 0x04, 0x16, 0x11, 0x19},
{0x09, 0x0c, 0x07, 0x03, 0x15, 0x10, 0x1a},
{none, 0x0d, 0x06, 0x02, 0x14, 0x18, none},
{none, none, 0x05, 0x01, 0x13, none, none},
{none, none, none, 0x00, none, none, none}
};

unsigned int a;

void printbits(uchar i){
int n = 0;
for(;n<8;n++)
p("%u", (i & (1 << (7-n))) == (1 << (7-n)));
p("\n");
}

void lshiftproc(fourbytes inout, uchar places){
uchar temp2;
uchar temp;
places = 3;
signed char oldplaces = places;
uchar p7 = places/7;
uchar l;

/*for(l=0;l<=p7;l++)*/while(oldplaces != 0){

if(oldplaces - 7 >= 0)
{
oldplaces -= 7;
places = 7;
}
else
{
places = oldplaces;
oldplaces = 0;
}

printf("%u\n\n", places);

temp = 0xff;
temp2 =0xff;

temp <<= places;
temp = ~(temp >> places);
temp &= inout[0];
temp >>= 8-places;
inout[0] <<= places;
//printbits(temp);

temp2 <<= places;
temp2 = ~(temp2 >> places);
temp2 &= inout[1];
temp2 >>= 8-places;
inout[1] <<= places;
inout[1] |= temp;
//printbits(temp2);

temp = 0xff;
temp <<= places;
temp = ~(temp >> places);
temp &= inout[2];
temp >>= 8-places;
inout[2] <<= places;
inout[2] |= temp2;
//printbits(temp);

inout[3] <<= places;
inout[3] |= temp;

}

}


void andproc(fourbytes inout, fourbytes by){
inout[0] = inout[0] & by[0];
inout[1] = inout[1] & by[1];
inout[2] = inout[2] & by[2];
inout[3] = inout[3] & by[3];
}
void orproc(fourbytes inout, fourbytes by){
inout[0] = inout[0] | by[0];
inout[1] = inout[1] | by[1];
inout[2] = inout[2] | by[2];
inout[3] = inout[3] | by[3];
}

void setproc(fourbytes inout, fourbytes by){
inout[0] = by[0];
inout[1] = by[1];
inout[2] = by[2];
inout[3] = by[3];
}

//uchar memcmp
fourbytes x = {0,0,0,0};
fourbytes temp = {0,0,0,0};
fourbytes onesource = {1,0,0,0};
fourbytes zero = {0,0,0,0};
fourbytes full = {0xff,0xff,0xff,0xff};

void setframe(frame in){
uchar m;
uchar n;
memset(x, 0, 4);
//for(m=0;m<6;m++)
//for(n=0;n<7;n++)
//{
setproc(temp, onesource);
lshiftproc(temp, 9);
orproc(x, temp);
printbits(x[0]);
printbits(x[1]);
printbits(x[2]);
printbits(x[3]);
//p("%2x, %2x, %2x, %2x", t)
//}
}

void buttonproc(){
setframe(test);
}

int main()
{
//    buttonproc();
  //  printf("%x", shiftmap[3][3]);
    buttonproc();
    return 0;
}
