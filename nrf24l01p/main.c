#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "nrf24l01p.h"

int main()
{
    uint8_t retval = 0x08; //value of config when powered up is always 8
    nr_config *conf = (nr_config*)&retval;
    printf("%u sizeof() = %u\n", conf->en_crc, sizeof(nr_config));
    return 0;
}
