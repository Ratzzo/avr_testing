#ifndef NRF24L01_H
#define NRF24L01_H

#include <stdint.h>

/*
    SPI commands, you may have to change some for compatibility
    check the table in the datasheet page 51
*/

//you have to or these two with the 5bit register addresses in order to access them
#define NR_R_REGISTER         0x0  //read reg
#define NR_W_REGISTER         0x20 //write reg


#define NR_R_RX_PAYLOAD       0x61
#define NR_W_TX_PAYLOAD       0xA0
#define NR_FLUSH_TX           0xE1
#define NR_FLUSH_RX           0xE2
#define NR_REUSE_TX_PL        0xE3
#define NR_R_RX_PL_WID        0x60
#define NR_W_ACK_PAYLOAD      0xA8 //you have to OR this one with the respect pipe number
#define NR_W_TX_PAYLOAD_NOACK 0xB0
#define NR_NOP                0xFF

/* register addresses
   Some of these may sound redundant; however,
*/

#define NR_ADDR_CONFIG      0x00 //Configuration Register
#define NR_ADDR_EN_AA       0x01 //Enable 'Auto Acknowledgment' Function. Disable this functionality to be compatible with nRF2401.
#define NR_ADDR_EN_RXADDR   0x02 //Enabled RX Addresses
#define NR_ADDR_SETUP_AW    0x03 //Setup of Address Widths
#define NR_ADDR_SETUP_RETR  0x04 //Setup of Automatic Retransmission
#define NR_ADDR_RF_CH       0x05 //RF Channel
#define NR_ADDR_RF_SETUP    0x06 //RF Setup Register
#define NR_ADDR_STATUS      0x07 //Status Register (In parallel to the SPI command
                                 //word applied on the MOSI pin, the STATUS register
                                 //is shifted serially out on the MISO pin
#define NR_ADDR_OBSERVE_TX  0x08 //Transmit observe register
#define NR_ADDR_RPD         0x09 //Received Power Detector
#define NR_ADDR_RX_ADDR_P0  0x0A //W Receive address data pipe 0. 5 Bytes maximum
                                 //length. (LSByte is written first. Write the number of
                                 //bytes defined by SETUP_AW)
#define NR_ADDR_RX_ADDR_P1  0x0B //Receive address data pipe 1. 5 Bytes maximum
                                 //length. (LSByte is written first. Write the number of
                                 //bytes defined by SETUP_AW)
#define NR_ADDR_RX_ADDR_P2  0x0C //Receive address data pipe 2. Only LSB. MSBytes are equal to RX_ADDR_P1[39:8]
#define NR_ADDR_RX_ADDR_P3  0x0D //Receive address data pipe 3. Only LSB. MSBytes are equal to RX_ADDR_P1[39:8]
#define NR_ADDR_RX_ADDR_P4  0x0E //Receive address data pipe 4. Only LSB. MSBytes are equal to RX_ADDR_P1[39:8]
#define NR_ADDR_RX_ADDR_P5  0x0F //Receive address data pipe 5. Only LSB. MSBytes are equal to RX_ADDR_P1[39:8]
#define NR_ADDR_TX_ADDR     0x10 //Transmit address. Used for a PTX device only
#define NR_ADDR_RX_PW_P0    0x11 //RX payload lenght information of pipe 0
#define NR_ADDR_RX_PW_P1    0x12 //RX payload lenght information of pipe 1
#define NR_ADDR_RX_PW_P2    0x13 //RX payload lenght information of pipe 2
#define NR_ADDR_RX_PW_P3    0x14 //RX payload lenght information of pipe 3
#define NR_ADDR_RX_PW_P4    0x15 //RX payload lenght information of pipe 4
#define NR_ADDR_RX_PW_P5    0x16 //RX payload lenght information of pipe 5
#define NR_ADDR_FIFO_STATUS 0x17 //FIFO Status Register
#define NR_ADDR_DYNPD       0x1C //Enable dynamic payload length
#define NR_ADDR_FEATURE     0x1D //Feature Register

#pragma pack(1, push)

//register structures for casting; check datasheet for details.
//pages 57-63

typedef struct {
    uint8_t prim_rx     : 1;
    uint8_t pwr_up      : 1;
    uint8_t crc0        : 1;
    uint8_t en_crc      : 1;
    uint8_t mask_max_rt : 1;
    uint8_t mask_tx_ds  : 1;
    uint8_t mask_rx_dr  : 1;
    uint8_t reserved    : 1;
} nr_config;

typedef struct {
    uint8_t enaa_p0     : 1;
    uint8_t enaa_p1     : 1;
    uint8_t enaa_p2     : 1;
    uint8_t enaa_p3     : 1;
    uint8_t enaa_p4     : 1;
    uint8_t enaa_p5     : 1;
    uint8_t reserved    : 2;
} nr_en_aa;

typedef struct {
    uint8_t erx_p0      : 1;
    uint8_t erx_p1      : 1;
    uint8_t erx_p2      : 1;
    uint8_t erx_p3      : 1;
    uint8_t erx_p4      : 1;
    uint8_t erx_p5      : 1;
    uint8_t reserved    : 2;
} nr_en_rxaddr;

typedef struct {
    uint8_t aw          : 2;
    uint8_t reserved    : 6;
} nr_setup_aw;

typedef struct {
    uint8_t arc         : 4;
    uint8_t ard         : 4;
} nr_setup_retr;

typedef struct {
    uint8_t rf_ch       : 7;
    uint8_t reserved    : 1;
} nr_rf_ch;

typedef struct {
    uint8_t obsolete    : 1;
    uint8_t rf_pwr      : 2;
    uint8_t rf_dr_high  : 1;
    uint8_t pll_lock    : 1;
    uint8_t rf_dr_low   : 1;
    uint8_t reserved    : 1;
    uint8_t cont_wave   : 1;
} nr_rf_setup;

typedef struct {
    uint8_t tx_full     : 1;
    uint8_t rx_p_no     : 3;
    uint8_t max_rt      : 1;
    uint8_t tx_ds       : 5;
    uint8_t rx_dr       : 1;
    uint8_t reserved    : 1;
} nr_status;

typedef struct {
    uint8_t arc_cnt     : 4;
    uint8_t plos_cnt    : 4;
} nr_observe_tx;

typedef struct {
    uint8_t rpd         : 1;
    uint8_t reserved    : 7;
} nr_rpd;

//0A, 0B, 0C, 0D, 0E, 0F, and 10 are 40bit (5 octet) addresses,
//we need to handle them with a specialized procedure

//these are shown in the datasheet as rx_addr_pX and tx_addr

typedef struct {
    uint8_t b0;
    uint8_t b1;
    uint8_t b2;
    uint8_t b3;
    uint8_t b4;
} nr_addr_t;

//these are for rx_pw_pX in the datasheet.

typedef struct {
    uint8_t rx_pw       : 6;
    uint8_t reserved    : 2;
} nr_rx_pw;

typedef struct {
    uint8_t rx_empty    : 1;
    uint8_t rx_full     : 1;
    uint8_t reserved    : 2;
    uint8_t tx_empty    : 1;
    uint8_t tx_full     : 1;
    uint8_t tx_reuse    : 1;
    uint8_t reserved2   : 1;
} nr_fifo_status;

typedef struct {
    uint8_t dpl_p0      : 1;
    uint8_t dpl_p1      : 1;
    uint8_t dpl_p2      : 1;
    uint8_t dpl_p3      : 1;
    uint8_t dpl_p4      : 1;
    uint8_t dpl_p5      : 1;
    uint8_t reserved    : 2;
} nr_dynp;

typedef struct {
    uint8_t en_dyn_ack  : 1;
    uint8_t en_ack_pay  : 1;
    uint8_t en_dpl      : 1;
    uint8_t reserved    : 5;
} nr_feature;

typedef struct {

} nr_t;

#pragma pack(pop)

//set required configuration state, and save it
int nr_begin()
{

}

int nr_end()
{

}

#endif // NRF24l01_H
