#include <stdio.h>
#include <stdint.h>
//damn
#include <windows.h>
#include <io.h>


//debug purposes only
/*
int setbaudrate(FILE *fp, uint32_t baudrate){
DCB d; //serial control structure
HANDLE fhandle = (void*)_get_osfhandle(_fileno(fp));
printf("getcommstate = %u\n", GetCommState(fhandle, &d));
d.BaudRate = baudrate;
SetCommState(fhandle, &d);
//_close(_fileno(fp));

}
*/

//change things here
int iwrite(void *buffer, size_t size, size_t count, HANDLE file){
    DWORD written = 0;
    WriteFile(file, buffer, size*count, &written, NULL);
    if(!written) return written;
    uint32_t i;
    for(i = 0; i < written; i++)
    printf("written: %x\n", ((uint8_t*)buffer)[i]);
    return written;
}

int iread(void *buffer, size_t size, size_t count, HANDLE file){
    DWORD read = 0;
    ReadFile(file, buffer, size*count, &read, NULL);
    if(!read) return read;
    uint32_t i;
    for(i = 0; i < read; i++)
    printf("read: %x\n", ((uint8_t*)buffer)[i]);
    return read;
}

#define iwrite(buffer, size) iwrite(buffer, size, 1, porthandle)
#define iread(buffer, size) iread(buffer, size, 1, porthandle)

//end debug purposes;
