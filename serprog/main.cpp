#include "debug.h"
#include <stdio.h>
#include <stdint.h>
#include "serprog.h"

#include <windows.h>
#include <io.h>
#include <FCNTL.h>

extern "C" {
extern HANDLE porthandle;
}



int main(){
    //port = fopen("\\\\.\\pipe\\magic", "rwb");
    //printf("pipe = %x\n", port);



    while(1){
    porthandle = CreateFile("\\\\.\\pipe\\magic", GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
    printf("handle = %x\n", porthandle);

      if (porthandle != INVALID_HANDLE_VALUE)
         break;

      // Exit if an error other than ERROR_PIPE_BUSY occurs.

      if (GetLastError() != ERROR_PIPE_BUSY)
      {
         printf( TEXT("Could not open pipe. GLE=%d\n"), GetLastError() );
         return -1;
      }

    }


    //setbaudrate(port, 9600);

    uint8_t buff[16];
    while(1){
        if(iread(buff, 1))
        handlecommand(buff[0]);
    }

   // fclose(port);

   CloseHandle(porthandle);

    return 0;
}
