#include "debug.h"
#include <stdio.h>
#include <stdint.h>
#include "serprog.h"
#include <windows.h>



HANDLE porthandle = 0;

uint8_t buffer[16];

#define writebyte(byte) {buffer[0] = byte; iwrite(buffer, 1);}
#define readbyte(byte) {iread(buffer, 1);}

#define b0(cmd) (1 << cmd)
#define b1(cmd) (1 << (cmd-8))



int handlecommand(uint8_t cmd){
    switch(cmd){
    case S_CMD_NOP:
       // Sleep(40);
        writebyte(S_ACK)
        break;
    case S_CMD_SYNCNOP:
       // Sleep(40);
        writebyte(S_NAK);
        writebyte(S_ACK);
        break;
    case S_CMD_Q_IFACE:
        writebyte(S_ACK);
        writebyte(0x1);
        writebyte(0x0);
        break;
    case S_CMD_Q_CMDMAP:
        writebyte(S_ACK);
        writebyte(b0(S_CMD_NOP) | b0(S_CMD_Q_IFACE) | b0(S_CMD_Q_CMDMAP) | b0(S_CMD_Q_PGMNAME | b0(S_CMD_Q_SERBUF) | b0(S_CMD_Q_BUSTYPE) | b0(S_CMD_O_SPIOP)));
        writebyte(0x00);
        writebyte(b1(S_CMD_));
        int i;
        for(i=0;i<29; i++)
        writebyte(0x0);
        break;
    case S_CMD_Q_BUSTYPE:
        writebyte(S_ACK);
        writebyte(S_BUSTYPE_SPI);
        break;

    }

    return 0;
}
