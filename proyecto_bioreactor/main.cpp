#include <Arduino.h>
#include <math.h>
#include <stdint.h>
#include <mcp23017.h>
#include <twi.h>
#include "dmui.h"
#include "keypad.h"

DisplayModuleUI *dp;

volatile int32_t motor_speed = 0;
volatile int32_t calentador = 0;
volatile uint32_t led2 = 0;

dmui_prop_t props[] = {
DEFPROP("Motor", DMUI_PROP_UINT32, DMUI_READWRITE, motor_speed, 0, 255),
DEFPROP("Calentador", DMUI_PROP_UINT32, DMUI_READWRITE, calentador, 0, 1),
//DEFPROP("Max_temp", DMUI_PROP_INT32, DMUI_READWRITE, led2, 0, 30),
DEFPROPEND
};

void setup()  {
	twi_init(400000L);
  //digitalWrite(SDA, 1);
  //digitalWrite(SCL, 1);
	Serial.begin(115200);
	pinMode(8, INPUT);
	pinMode(13, OUTPUT);
	pinMode(7, OUTPUT);
	pinMode(11, OUTPUT);
	pinMode(2, OUTPUT);
	pinMode(3, OUTPUT);
	dp = new DisplayModuleUI(8, 0, props);
	digitalWrite(7, HIGH);
	//dp->cb = keyhandler;
}

void loop()  {
	dp->checkForEvent();
	analogWrite(11, motor_speed);
	digitalWrite(2, calentador);
	digitalWrite(3, calentador);
	
}
