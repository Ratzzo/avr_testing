#include "Arduino.h"
#include <twi.h>
#include <pcf8574.h>
#include <stdint.h>

pcf8574_t pcf;

void setup(){
	Serial.begin(115200);
	twi_init(400000);
	pcf8574_create(&pcf, 0);
}

uint8_t mask = 0b01010000; //default state where all mosfets are turned off

void loop(){
	pcf8574_setbyte(&pcf, mask); //dead time
	pcf8574_setbyte(&pcf, mask ^ 0b10010000);
	delay(150);
	pcf8574_setbyte(&pcf, mask); //dead time
	pcf8574_setbyte(&pcf, mask ^ 0b01100000);
	delay(100);
}