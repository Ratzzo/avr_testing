#include <Arduino.h>
#include <math.h>
#include <stdint.h>
#include <mcp23017.h>
#include <twi.h>
#include "dmui.h"
#include "keypad.h"

#define distanceperstep 0.021333333f

volatile uint8_t step_forward = 0;
volatile uint32_t step_interval = 8000;
volatile uint32_t minsteps = 100;
volatile double maxsteps = minsteps + 2343;
volatile uint32_t nsteps = 0;

volatile uint32_t timer1;

int stepper_pins[4] = {2,3,4,7}; 

typedef struct {
uint8_t p1 : 2;
uint8_t p2 : 2;
uint8_t p3 : 2;
uint8_t p4 : 2;
} step_t;

step_t stepmap[] = {
{1, 0, 0, 0},
{1, 1, 0, 0},
{0, 1, 0, 0},
{0, 1, 1, 0},
{0, 0, 1, 0},
{0, 0, 1, 1},
{0, 0, 0, 1},
{1, 0, 0, 1}
};

void previousstep();
void nextstep();

void setup()  {
	twi_init(400000L);
	Serial.begin(115200);
	//stepper pins
	pinMode(8, INPUT);
	for(unsigned int i = 0; i < sizeof(stepper_pins)/sizeof(int); i++){
		pinMode(stepper_pins[i], OUTPUT);
		digitalWrite(stepper_pins[i], LOW);
	}
	timer1 = micros();
	//dp->cb = keyhandler;
	
	//zero
	while(!digitalRead(8)){
		previousstep();
		delayMicroseconds(1500);
	}
	
	nsteps = 0;
	
	//set position
	while(nsteps <= minsteps){
		nextstep();
		delayMicroseconds(1500);
	}
}

static int stepindex = 0;

void previousstep(){
	nsteps--;
	stepindex++;
	if(stepindex > 7) stepindex = 0;
	if(stepindex < 0) stepindex = 7;
	digitalWrite(stepper_pins[0], stepmap[stepindex].p1);
	digitalWrite(stepper_pins[1], stepmap[stepindex].p2);
	digitalWrite(stepper_pins[2], stepmap[stepindex].p3);
	digitalWrite(stepper_pins[3], stepmap[stepindex].p4);
}

void nextstep(){
	nsteps++;
	stepindex--;
	if(stepindex < 0) stepindex = 7;
	if(stepindex > 7) stepindex = 0;
	digitalWrite(stepper_pins[0], stepmap[stepindex].p1);
	digitalWrite(stepper_pins[1], stepmap[stepindex].p2);
	digitalWrite(stepper_pins[2], stepmap[stepindex].p3);
	digitalWrite(stepper_pins[3], stepmap[stepindex].p4);
}

void loop()  {
	
	if(nsteps >= maxsteps) {
		nsteps = maxsteps;
		step_forward = 0;
	} else if(nsteps <= minsteps) {
		nsteps = minsteps;
		step_forward = 1;
	}
	if(micros() > timer1 + step_interval){
		if(step_forward)
			nextstep();
			else
			previousstep();
		timer1 = micros();
	}
}
