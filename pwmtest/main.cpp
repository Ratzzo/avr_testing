#include <Arduino.h>
#include <twi.h>
//#include <SPI.h>
#include <mcp23017.h>
#include <avr/io.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdint.h>

int led = 13;
int pwm2 = 5;
int analoginput = A0; 

// the setup routine runs once when you press reset:
void setup() {
  // initialize the digital pin as an output.
  Serial.begin(115200);
  pinMode(led, OUTPUT);
  pinMode(led, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  uint16_t val = (analogRead(analoginput)/4);
  analogWrite(pwm2, val);
  char text[16];
  sprintf(text, "%u", val);
  Serial.println(text);
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);               // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(100);               // wait for a second
}
