#include "Arduino.h"
#include <twi.h>
#include <pcf8574.h>
#include <stdint.h>

pcf8574_t pcf;
pcf8574_t pcf2;

//			
//		6	
//	5		7
//		4	
//	3		1
//		2		0

uint8_t numbers[] = {
	(uint8_t)~(1 << 2 | 1 << 3 | 1 << 1 | 1 << 5 | 1 << 7 | 1 << 6), 		 // 0
	(uint8_t)~(1 << 1 | 1 << 7),												 // 1
	(uint8_t)~(1 << 6 | 1 << 7 | 1 << 4 | 1 << 3 | 1 << 2),					 // 2
	(uint8_t)~(1 << 6 | 1 << 7 | 1 << 4 | 1 << 1 | 1 << 2),					 // 3
	(uint8_t)~(1 << 5 | 1 << 7 | 1 << 4 | 1 << 1),           			     // 4
	(uint8_t)~(1 << 6 | 1 << 5 | 1 << 4 | 1 << 1 | 1 << 2), 					 // 5
	(uint8_t)~(1 << 5 | 1 << 4 | 1 << 3 | 1 << 1 | 1 << 2), 					 // 6
	(uint8_t)~(1 << 6 | 1 << 7 | 1 << 1), 									 // 7
	(uint8_t)~(1 << 2 | 1 << 3 | 1 << 1 | 1 << 5 | 1 << 7 | 1 << 6 | 1 << 4), // 8
	(uint8_t)~(1 << 5 | 1 << 6 | 1 << 7 | 1 << 4 | 1 << 1),					 // 9
};


void setup(){
	Serial.begin(115200);
	twi_init();
	pcf8574_create(&pcf, 0);
	pcf8574_create(&pcf2, 1);
}

int biet = 0;

void loop(){
	pcf8574_setbyte(&pcf, numbers[biet % 10]);
	pcf8574_setbyte(&pcf2, numbers[biet/10]);
	biet--;
	if(biet == -1)
		biet = 9;
	delay(1000);
}