#include "driver_595.h"

//low level shit
//#define driver_set_direction(dir, pin, state) _SFR_IO8(dir) 
//#define driver_setstate(port, pin, state) 

static inline void driver_set_pin_state(driver_ppd_t *ppd, uint8_t state){
	uint8_t *reg = ppd->port;
	*reg = (*reg & (0xff ^ 1 << ppd->pin)) | (state << ppd->pin);
}


int driver_595_create(driver_595_t *driver, uint8_t *shcp_port, uint8_t shcp_pin, uint8_t *stcp_port, uint8_t stcp_pin, uint8_t *ds_port, uint8_t ds_pin){
	driver->shcp.port = shcp_port;
	driver->shcp.pin = shcp_pin;
	driver->stcp.port = stcp_port;
	driver->stcp.pin = stcp_pin;
	driver->ds.port = ds_port;
	driver->ds.pin = ds_pin;
	
	return 0;
}

//serialize data into a port 

int driver_595_write(driver_595_t *driver, uint8_t data){
	//do the magic stuff
	//8 bits
	driver->byte = data;
	driver_set_pin_state(&driver->stcp, 0);
	int i;
	for(i = 0; i < 8; i++){
		driver_set_pin_state(&driver->shcp, 0); //shcp low
		driver_set_pin_state(&driver->ds, data & 1); //grab the first bit, and send it to ds
		driver_set_pin_state(&driver->shcp, 1); //shcp high
		
		data >>= 1; //shift data
	}
	
	driver_set_pin_state(&driver->stcp, 1);
	driver_set_pin_state(&driver->stcp, 0);
	driver_set_pin_state(&driver->shcp, 0); //shcp low
	return 0;
}	