#include <avr/io.h>
#include <util/delay.h>
#include "driver_595.h"
#include <stdint.h>
driver_595_t driver;
static uint8_t nblinkenlights = 0;

#define blinkenlight_item_size 5
uint8_t dasblinkenlichten[] = { //right to left
    0, 0, 0, 0, 20, //pin one on GPIOA //BLUE
    0, 1, 0, 0, 20,//pin one on GPIOA //GREEN
    0, 2, 0, 0, 20,//pin one on GPIOA //GREEN
    0, 3, 0, 0, 20,//pin one on GPIOA //GREEN
    0, 4, 0, 0, 20,//pin one on GPIOA //GREEN
    0, 5, 0, 0, 20,//pin one on GPIOA //GREEN
    0, 6, 0, 0, 20,//pin one on GPIOA //GREEN
    0, 7, 0, 0, 20,//pin one on GPIOA //GREEN
    };

	
int util_init(){
    nblinkenlights = sizeof(dasblinkenlichten)/blinkenlight_item_size;
	return 0;
}

int util_statusled_set2(uint8_t which, uint8_t state, int accumulator, int accumax, uint8_t clearblink){
    uint8_t *ind = dasblinkenlichten + which*sizeof(uint8_t)*blinkenlight_item_size; //hahahahaha!
    /*uint8_t reg = */*ind++;
    uint8_t shift = *ind++;
    uint8_t *s_state = ind++;
    uint8_t *s_accu = ind++;
    uint8_t *s_accumax = ind++;
    uint8_t byte = 0;

//    util_log_int(reg);

    if(state > 1 || clearblink){
        if(accumulator > -1) *s_accu = accumulator;
        if(accumax > 0) *s_accumax = accumax;
        *s_state = state;
        return 0;
    }

    byte = driver_595_read(&driver);
    byte = state ? (byte | (1 << shift)) : (byte & (0xff ^ (1 << shift)));
    driver_595_write(&driver, byte);

    return 0;
}

void tempo(){
    int i;
    uint8_t *base = dasblinkenlichten;
    for(i = 0;i < nblinkenlights;i++){
        /*uint8_t *gpio = */base++;
        /*uint8_t *io = */base++;
        uint8_t *state = base++;
        uint8_t *accumulator = base++;
        uint8_t *accumax = base++;
        if(*state > 1){

            if(*accumulator == *state - 1){
                //turn off
                util_statusled_set2(i, 0, -1, -1, 0);
            } else if(*accumulator == *accumax) {
                //turn on
                util_statusled_set2(i, 1, -1, -1, 0);
                *accumulator = 0;
            }


            (*accumulator)++;
        }
    }
}

int main()  {
	uint8_t caca = 0;
	DDRB |= 1 << DDB0; 
	DDRB |= 1 << DDB1; 
	DDRB |= 1 << DDB2; 
	util_init();
	driver_595_create(&driver, &PORTB, 0, &PORTB, 1, &PORTB, 2);
	util_statusled_set2(0, 3, 0, 3, 0);
    util_statusled_set2(1, 3, 0, 4, 0);
    util_statusled_set2(2, 3, 0, 5, 0);
    util_statusled_set2(3, 3, 0, 6, 0);
    util_statusled_set2(4, 3, 0, 7, 0);
    util_statusled_set2(5, 3, 0, 8, 0);
    util_statusled_set2(6, 3, 0, 9, 0);
    util_statusled_set2(7, 3, 0, 10, 0);
	//driver_595_write(&driver, 3);
	//uint8_t *portb = &PORTB;
	while(1){
		//*portb ^= 1 << PB0;
		tempo();
		//driver_595_write(&driver, caca);
		//caca++;
		_delay_ms(100);
	}
}