#ifndef DRIVER_595_H
#define DRIVER_595_H

#include <stdint.h>
#include <avr/io.h>

typedef struct driver_ppd{ /* pin number plus port and direction registers*/
	uint8_t pin; 
	uint8_t *port; //pointers to registers obtained from, ie &PORTB on avr
} driver_ppd_t;

typedef struct driver_595_t {
	driver_ppd_t shcp;
	driver_ppd_t stcp;
	driver_ppd_t ds;
	uint8_t byte;
} driver_595_t;

int driver_595_create(driver_595_t *driver, uint8_t *shcp_port, uint8_t shcp_pin, uint8_t *stcp_port, uint8_t stcp_pin, uint8_t *ds_port, uint8_t ds_pin);
int driver_595_write(driver_595_t *driver, uint8_t data);
#define driver_595_read(drv) ((drv)->byte)

#endif