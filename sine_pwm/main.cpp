#include <Arduino.h>
#include <math.h>
#include <stdint.h>

#define LED 13
#define SINEPWM 5

unsigned long ms = 0;
unsigned long ms2 = 0;

void setup()  {
	Serial.begin(115200);
	pinMode(LED, OUTPUT);
	pinMode(SINEPWM, OUTPUT);
	ms = millis();
	ms2 = millis();
}

bool i = 0;
int32_t x = 0;
float b = 0;

double val = 0;

void loop()  {
	if(millis() > ms + 10){
	b += 0.01;
	val = 127 + 127*sin(b);
		analogWrite(SINEPWM, val);
		//digitalWrite(5, 1);
		ms = millis();
	}
	if(millis() > ms2 + 1000){
		Serial.println(val);
		ms2 = millis();
		digitalWrite(LED, i ^= 1);
	}
}
