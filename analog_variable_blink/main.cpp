#include <Arduino.h>
#include <stdint.h>

#define LED 13
#define AINPUT A1

unsigned long ms = 0;
unsigned long ms2 = 0;

void setup()  {
	Serial.begin(115200);
	pinMode(LED, OUTPUT);
	pinMode(AINPUT, INPUT);
	ms = millis();
	ms2 = millis();
}

bool i = 0;

void loop()  {
	//uint32_t val = 350 - ((analogRead(AINPUT)-500)*10)/16;
	uint32_t val = 1000;
	if(millis() > ms + val){
//Serial.println(val);
		i ^= 1;
		digitalWrite(LED, i);
		ms = millis();
	}
	if(millis() > ms2 + 1000){
		Serial.println(sizeof(val));
		ms2 = millis();
	}
}
