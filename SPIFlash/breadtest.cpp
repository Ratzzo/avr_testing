#include <stdio.h>
#include <stdint.h>
#include <Arduino.h>
#include <SPI.h>

void p(char *fmt, ... ){
        char tmp[128]; // resulting string limited to 128 chars
        va_list args;
        va_start (args, fmt );
        vsnprintf(tmp, 128, fmt, args);
        va_end (args);
        Serial.print(tmp);
}

void printbits(uint8_t i){
int n = 0;
for(;n<8;n++)
p("%u", (i & (1 << (7-n))) == (1 << (7-n)));
p("\n");
}

void setup(){
DDRB |= (1<<DDB1);
PORTB |= (1<<PORTB1);
//DDRB |= (1<<DDB5);
//PORTB |= (1<<PORTB5);

SPI.setDataMode(SPI_MODE0);
SPI.setBitOrder(MSBFIRST);
//SPI.setClockDivider(SPI_CLOCK_DIV1);
SPI.begin();
Serial.begin(9600);
}


void loop(){
uint8_t sregister = 0x01;
uint8_t sregister2 = 0x02;
uint8_t sregister3 = 0x03;
uint8_t sregister4 = 0x04;
uint8_t sregister5 = 0x05;
//PORTB ^= (1<<PORTB5);
//delay(200);
PORTB &= ~(1<<PORTB1); //select
//delay(10);
SPI.transfer(0xAB); //read status register
sregister2 = SPI.transfer(0x00);
sregister3 = SPI.transfer(0x00);
sregister4 = SPI.transfer(0x00);
sregister5 = SPI.transfer(0x00);
PORTB |= (1<<PORTB1); //deselect
printbits(sregister);
printbits(sregister2);
printbits(sregister3);
printbits(sregister4);
printbits(sregister5);
p("\n");
//p("caca\n");

delay(1000);
}