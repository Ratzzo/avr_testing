
#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

//defines needed for Atmega328: 
#define    UCSRA    UCSR0A 
#define    UCSRB    UCSR0B 
#define    UCSRC    UCSR0C 
#define    UBRRH    UBRR0H 
#define    UBRRL    UBRR0L 
#define    UDRE    UDRE0 
#define    UDR    UDR0 
#define    RXC    RXC0 
#define    RXEN    RXEN0 
#define    TXEN    TXEN0 
#define    UCSZ1    UCSZ01 
#define    UCSZ0    UCSZ00 
#define output_high(port,pin) port |= (1<<pin) 
#define output_low(port,pin) port &= ~(1<<pin) 
#define set_output(portdir,pin) portdir |= (1<<pin) 

extern uint32_t millis();
extern void millis_init(void);

int main()
{
	//InitUART(0x0);
	//stdout = &mystdout;
	millis_init();
	sei();
    DDRB |= 1 << DDB1;  //set ddb1 as output
	
	uint32_t m = 0;
	
	uint8_t outports[] = {DDC4, DDC3, DDC2, DDC1, DDC0, DDC5};
	uint8_t inports[] =  {DDD0, DDD1, DDD2, DDD3, DDD4, DDD7};
	uint8_t outtypes[] = {0, 0, 1, 1, 1, 1}; //0 = flip flop, 1 = push button
	uint8_t inverted[] = {0, 0, 0, 0, 1, 1};
	uint8_t locks[] = 	 {0, 0, 0, 0, 0, 0}; //pin locks
	uint32_t millises[] = {0, 0, 0, 0, 0, 0};
	
	for(m = 0; m < sizeof(outports); m++)
		DDRC |= 1 << outports[m];  //set ddb4 as output

	for(m = 0; m < sizeof(inports); m++)
		DDRD &= (1 << inports[m]) ^ 0xff; //set dd7 as input
	
	PORTD = 0xff; //pullup port D
	

	uint32_t ms = 0;
	uint32_t val = 90;
	uint32_t current_millis;
	uint8_t lastpind = PIND;
	
	uint8_t lock = 0;
	uint32_t millise = 0;
    while(1){
		current_millis = millis();
		if(current_millis >  ms + val){
			PORTB ^= _BV(PB1);
			ms = millis();
		}
		
		//watch and debounce.
		
		
		if(PIND != lastpind){ //something changed
			
			for(m = 0; m < sizeof(inports); m++){
				if(outtypes[m] == 0){
						if(!(PIND & (1 << inports[m])) && !locks[m]) { //ddd0 pressed and not locked
							locks[m] = 1; //lock it
						}
						if(locks[m] && millis() > millises[m] + 100 && (PIND & (1 << inports[m]))){
							PORTC ^= _BV(outports[m]); //toggle
							locks[m] = 0;
							millises[m] = millis();
						}
				}
				else
				{
					//reflect input to output
					if(inverted[m] != !(PIND & (1 << inports[m]))){
						PORTC |= 1 << outports[m];
					}
					else
					{
						PORTC &= ~(0xff & (1 << outports[m]));
					}
					
				}
				
			}
			
			lastpind = PIND;
		}
		

    }
}