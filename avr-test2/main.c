#include "core/arduino.h"
#include "../libraries/SoftwareSerial/SoftwareSerial.h"

/* remember linker op --gc-sections and compiler ops -fdata-sections -ffunction-sections */


typedef unsigned char uchar;

uchar setupledpin;    // LED connected to digital pin 9
uchar mgmtconenablepin;

uchar mgmtcon_commandbuff[256];
int mgmtcon_commandbuffpos = 0;

uchar mgmtconlock = 0;

void setup()  {
  setupledpin = A5;
  mgmtconenablepin = 8;
  memset(mgmtcon_commandbuff, 0, 256);
  Serial.begin(9600);
  pinMode(setupledpin, OUTPUT);
  pinMode(mgmtconenablepin, INPUT);
  digitalWrite(setupledpin, HIGH); //turn on the led in signal of everything fine
}

int eventcheck(){
uchar mgmtconenable = digitalRead(mgmtconenablepin);
if(mgmtconenable && !mgmtconlock) //enable management console
{
Serial.write("Management console:\n");

mgmtconlock = 1;
return 1;
}

if(mgmtconlock){ //if management console is enabled receive commands
//Serial.
}

return 0;
}

void loop()  {
eventcheck();
}
