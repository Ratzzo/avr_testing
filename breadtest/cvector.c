#include <stdlib.h>
#include <string.h>

/*
TODO: add error checking, when error occurs restore the previous state undoing the changes done to the vector object -- DONE!
TODO: create a lightweight-enabled version, not using the odd index table that takes 4 bytes in 32 and 8 in 64 per element, instead
of using the table it should use it's own functions to get elements, like "getelement(uchar *out, uint index) - DONE!!"

}

*/

#include "cvector.h"

#ifndef CVECTOR_LIGHTWEIGHT
#define checkitemsallocation(addc) if(vec->items == 0) \
{ \
addc \
     return 1; \
}

#endif

#define checkitemsmemallocation(addc) if(vec->itemsmem == 0){ \
addc \
return 2; \
}


uint cvector_allocate(cvector *vec, uint itemsize, uint length)  //allocate constant size vector
{

#ifndef CVECTOR_LIGHTWEIGHT
    vec->items = (uchar*)malloc(length*sizeof(uchar*)); //allocated the pointer array, bla bla, I could save this memory for other use

#ifndef CVECTOR_NOIF
    checkitemsallocation();
#endif

#endif

    vec->itemsmem = (uchar*)malloc(itemsize*length); //here items will be stored rawly
    memset(vec->itemsmem, 0, itemsize*length);
#ifndef CVECTOR_NOIF
    checkitemsmemallocation();
#endif

#ifndef CVECTOR_LIGHTWEIGHT
    uint m;
    for(m=0; m<length; m++)
    {

        vec->items[m] = vec->itemsmem+(m*itemsize); //put the addreses of each vector in its place

    }
#endif

    vec->len = length;
    vec->itemsize = itemsize;

    return 0;
}

uint cvector_setlength(cvector *vec, uint newlen)  //set vector length, reallocate the entire vector element, this is faster than pushing things
{

    uint oldlen = vec->len;

    vec->len = newlen;

#ifndef CVECTOR_LIGHTWEIGHT

#ifndef CVECTOR_NOIF
    uchar **olditemsptr = vec->items;
#endif

    vec->items = (uchar*)realloc(vec->items, vec->len * sizeof(uchar*));
#ifndef CVECTOR_NOIF
    if(newlen != 0)
        checkitemsallocation(
            vec->len = oldlen;
            vec->items = olditemsptr;
        );
#endif
#endif

    uchar *vecdataold = vec->itemsmem; //check if this changes next

    vec->itemsmem = (uchar*)realloc(vec->itemsmem, vec->len * vec->itemsize); //reallocate itemsmem
#ifndef CVECTOR_NOIF
    if(newlen != 0)
        checkitemsmemallocation(
            vec->len = oldlen;
            vec->itemsmem = vecdataold;
#ifndef CVECTOR_LIGHTWEIGHT
            vec->items = (uchar*)realloc(vec->items, oldlen * sizeof(uchar*));
#endif
        );
#endif

#ifndef CVECTOR_LIGHTWEIGHT
    if(vecdataold != vec->itemsmem)  //if address changed
    {
        uint i;
        for(i=0; i < newlen; i++)
        {
            vec->items[i] = vec->itemsmem + i*vec->itemsize;
        }
    }
    else //id address didn't changed
    {
        uint i;
        for(i=oldlen; i < newlen; i++)
        {
            vec->items[i] = vec->itemsmem + i*vec->itemsize;
        }
    }
#endif

    return 0;
}

uint cvector_push(cvector *vec, void *data)  //push data to the vector, size increased by 1 and data put at the top ---->
{
//if(vec == 0) return 1; // there's no vector!!
//if(data == 0) return 2; // there's no data!!
    vec->len += 1; //increment vector size

#ifndef CVECTOR_LIGHTWEIGHT
    uchar **itemsold = vec->items;
    vec->items = (uchar*)realloc(vec->items, vec->len * sizeof(uchar*)); //reallocate pointer index
#ifndef CVECTOR_NOIF
    checkitemsallocation(
        vec->items = itemsold;
        vec->len--;
    );
#endif
#endif

    uchar *itemsmemold = vec->itemsmem; //if this changes with the realloc we have to update the items table


    vec->itemsmem = (uchar*)realloc(vec->itemsmem, vec->itemsize * vec->len); //reallocate memory
#ifndef CVECTOR_NOIF
    checkitemsmemallocation(
        vec->itemsmem = itemsmemold;
        vec->len--;
        #ifndef CVECTOR_LIGHTWEIGHT
        vec->items = (uchar*)realloc(vec->items, vec->len * sizeof(uchar*));
        #endif
    );
#endif


    uint currindex = vec->len - 1; //set current idex now
#ifndef CVECTOR_LIGHTWEIGHT
    if(itemsmemold != vec->itemsmem)  //update the table
    {
        uint curritem;
        for(curritem=0; curritem < vec->len; curritem++)
        {
//printf("%u\n", curritem*vec->itemsize);
            vec->items[curritem] = vec->itemsmem + (curritem*vec->itemsize);
//printf("%x\n", vec->itemsmem + (vec->itemsize*curritem));
        }
    }
    else
    {
        vec->items[currindex] = vec->itemsmem + (currindex*vec->itemsize);
    }

    memcpy(vec->items[currindex],data, vec->itemsize); //copy memory and fill data
#endif

#ifdef CVECTOR_LIGHTWEIGHT
    memcpy(vec->itemsmem +currindex*vec->itemsize, data, vec->itemsize);
#endif
    return 0;
}

uint cvector_pop(cvector *vec, void *data)  //pop data from the vector
{
//if(vec == 0) return 1; // there's no vector!!
//if(data == 0) return 2; // there's no data!!
#ifndef CVECTOR_NOIF
    if(vec->len == 0) return 1;
#endif
    vec->len -= 1; //increment vector size
    if(data)
    memcpy(data,
#ifndef CVECTOR_LIGHTWEIGHT
vec->items[vec->len]
#else
vec->itemsmem + vec->len*vec->itemsize
#endif
, vec->itemsize);


#ifndef CVECTOR_LIGHTWEIGHT
#ifndef CVECTOR_NOIF
    uchar **itemsold = vec->items;
#endif

    vec->items = (uchar*)realloc(vec->items, vec->len * sizeof(uchar*)); //reallocate pointer index
#ifndef CVECTOR_NOIF
    if(vec->len != 0)
        checkitemsallocation(
            vec->len++;
            vec->items = itemsold;
        );
#endif
#endif

    uchar *olditemsmem = vec->itemsmem;

    vec->itemsmem = (uchar*)realloc(vec->itemsmem, vec->itemsize * vec->len); //reallocate memory
#ifndef CVECTOR_NOIF
    if(vec->len != 0)
        checkitemsmemallocation(
            vec->len++;
            vec->itemsmem = olditemsmem;
            #ifndef CVECTOR_LIGHTWEIGHT
            vec->items = (uchar*)realloc(vec->items, vec->len * sizeof(uchar));
            uint i;
            for(i=0; i<vec->len; i++) //update table
            vec->items[i] = vec->itemsmem + vec->len*i;
        #endif
        );
#endif

#ifndef CVECTOR_LIGHTWEIGHT
#ifdef CVECTOR_UPDATEONREALLOC
    if(olditemsmem != vec->itemsmem)
    {
        uint i;
        for(i = 0; i < vec->len; i++)
            vec->items[i] = vec->itemsmem + vec->itemsize*i;
    }
#endif
#endif

    return 0;
}

uint cvector_insert(cvector *vec, void *data, uint index) //insert data in vec at index
{
#ifndef CVECTOR_NOIF
    if(index >= vec->len) return 3;
#endif

    vec->len += 1;

#ifndef CVECTOR_LIGHTWEIGHT
#ifndef CVECTOR_NOIF
    uchar **itemsold = vec->items;
#endif

    vec->items = (uchar*)realloc(vec->items, vec->len * sizeof(uchar*));
#ifndef CVECTOR_NOIF
    checkitemsallocation(
        vec->items = itemsold;
        vec->len--;
    );
#endif
#endif
    uchar *vecdataold = vec->itemsmem; //check if this changes next

    vec->itemsmem = (uchar*)realloc(vec->itemsmem, vec->len * vec->itemsize); //reallocate itemsmem
#ifndef CVECTOR_NOIF
    checkitemsmemallocation(
        vec->len--;
        vec->itemsmem = vecdataold;
#ifndef CVECTOR_LIGHTWEIGHT
        vec->items = (uchar*)realloc(vec->items, vec->len * sizeof(uchar*));
#endif
    )
#endif

    uint oldlen = vec->len - 1;

#ifndef CVECTOR_LIGHTWEIGHT
    if(vecdataold != vec->itemsmem)  //if address changed
    {
        uint i = 0;
        for(; i < vec->len; i++)
        {
            vec->items[i] = vec->itemsmem + i*vec->itemsize;
        }

    }
    else //id address didn't changed
    {
        vec->items[oldlen] = vec->itemsmem + oldlen*vec->itemsize;
    }

#endif

//now move the memory

    uint i = oldlen;
    for(; i > index; i--)
    {
//printf("%x\n\n", vec->items[oldlen]);
#ifndef CVECTOR_LIGHTWEIGHT
        memcpy(vec->items[i], vec->items[i-1], vec->itemsize); //push everything
#else
        memcpy(vec->itemsmem + i*vec->itemsize, vec->itemsmem + (i-1)*vec->itemsize, vec->itemsize); //push everything
#endif
    }

#ifndef CVECTOR_LIGHTWEIGHT
    memcpy(vec->items[index], data, vec->itemsize);
#else
    memcpy(vec->itemsmem + index*vec->itemsize, data, vec->itemsize);
#endif
    return 0;
}

uint cvector_delete(cvector *vec, uint index)  //todo
{
#ifndef CVECTOR_NOIF
    if(vec->len == 0) return 1;
    if(index >= vec->len) return 2;
#endif
    vec->len -= 1;

    uint i;
//printf("for\n");

    for(i = index; i < vec->len; i++)
    {
        #ifndef CVECTOR_LIGHTWEIGHT
        memcpy(vec->items[i], vec->items[i+1], vec->itemsize);
        #else
        memcpy(vec->itemsmem + i*vec->itemsize, vec->itemsmem + (i+1)*vec->itemsize, vec->itemsize);
        #endif
    }
//printf("endfor");
#ifndef CVECTOR_LIGHTWEIGHT
#ifndef CVECTOR_NOIF
    uchar **itemsold = vec->items;
#endif

    vec->items = (uchar*)realloc(vec->items, vec->len * sizeof(uchar*));
#ifndef CVECTOR_NOIF
    checkitemsallocation(
        vec->len++;
        vec->items = itemsold;
    );
#endif
#endif

    uchar *olditemsmem = vec->itemsmem;

    vec->itemsmem = (uchar*)realloc(vec->itemsmem, vec->len * vec->itemsize);

#ifndef CVECTOR_NOIF
    checkitemsmemallocation(
        vec->len++;
        vec->itemsmem = olditemsmem;
        #ifndef CVECTOR_LIGHTWEIGHT
        vec->items = (uchar*)realloc(vec->items, vec->len * sizeof(uchar*));

        uint i;


        for(i=0; i<vec->len; i++) //update table
        vec->items[i] = vec->itemsmem + vec->len*i;
        #endif
    );
#endif

#ifndef CVECTOR_LIGHTWEIGHT
#ifdef CVECTOR_UPDATEONREALLOC
    if(olditemsmem != vec->itemsmem)
    {
        uint i;
        for(i=0; i<vec->len; i++)
            vec->items[i] = vec->itemsmem + vec->len*i;
    }
#endif
#endif

    return 0;
}

uint cvector_xchg(cvector *vec, uint src, uint dest) //exchange items, exchange src and dest in vec
{
    //using the xchg x86 instruction would be faster, but I need the code portable
#ifndef CVECTOR_NOIF
    if(dest >= vec->len || src >= vec->len) return 1;
#endif
    uchar *temp = (uchar*)malloc(vec->itemsize);
#ifndef CVECTOR_NOIF
    if(temp == 0) return 2;
#endif
#ifndef CVECTOR_LIGHTWEIGHT
    memcpy(temp, vec->items[dest], vec->itemsize);
    memcpy(vec->items[dest], vec->items[src], vec->itemsize);
    memcpy(vec->items[src], temp, vec->itemsize);
#else
    memcpy(temp, vec->itemsmem + dest*vec->itemsize, vec->itemsize);
    memcpy(vec->itemsmem + dest*vec->itemsize, vec->itemsmem + src*vec->itemsize, vec->itemsize);
    memcpy(vec->itemsmem + src*vec->itemsize, temp, vec->itemsize);
#endif
    free(temp);
    return 0;
}

uchar* cvector_getitem(cvector *vec, uint index){
#ifndef CVECTOR_LIGHTWEIGHT
//memcpy(out, vec->items[index], vec->itemsize);
return (uchar*)vec->items[index];
#else
//memcpy(out, vec->itemsmem + index*vec->itemsize, vec->itemsize);
return (uchar*)(vec->itemsmem + index*vec->itemsize);
#endif
return 0;
}

uint cvector_setitem(cvector *vec, void* in, uint index){
#ifndef CVECTOR_LIGHTWEIGHT
memcpy(vec->items[index], in, vec->itemsize);
#else
memcpy(vec->itemsmem + index*vec->itemsize, in, vec->itemsize);
#endif
return 0;
}

uint cvector_destroy(cvector *vec)  //clean up for preventing memory leaks
{
    free(vec->itemsmem);
    #ifndef CVECTOR_LIGHTWEIGHT
    free(vec->items);
    #endif
    return 0;
}
