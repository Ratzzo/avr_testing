#include <Arduino.h>
//#include <SPI.h>
#include "cvector.h"

typedef unsigned char uchar;
typedef unsigned int uint;

const uchar setupledpin = A5;    // LED connected to digital pin 9
const uchar mgmtconenablepin = 8;

#define CMDBUFSIZE 256

uchar mgmtcon_commandbuff[CMDBUFSIZE];
int mgmtcon_commandbuffpos = 0;

uchar mgmtconlock = 1;

void setup()  {
  memset(mgmtcon_commandbuff, 0, CMDBUFSIZE);
  Serial.begin(9600);
  pinMode(setupledpin, OUTPUT);
  pinMode(mgmtconenablepin, INPUT);
  digitalWrite(setupledpin, HIGH); //turn on the led in signal of everything fine
}

void p(char *fmt, ... ){
        char tmp[128]; // resulting string limited to 128 chars
        va_list args;
        va_start (args, fmt );
        vsnprintf(tmp, 128, fmt, args);
        va_end (args);
        Serial.print(tmp);
}

int commandprocessor(uint argc, uchar *argv[]){
uint i;
for(i=0;i<argc;i++)
p("%s ", argv[i]);
p("\n");

if(!strcmp((char*)argv[0], "pinmode")){ //pinmode command: pinmode <pin> <in | out>
    uint pin = 2;
    uchar mode = 0;
    if(argc < 3) goto pinmodeerror; //number of arguments
    pin = atoi((char*)argv[1]);
    if(!strcmp((char*)argv[2], "in"))
        mode = 0;
    else if(!strcmp((char*)argv[2], "out"))
        mode = 1;
    else goto pinmodee//error;
    goto pinmodenoerror;

    pinmodeerror:
    p("pinmode <pin> <in | out>\n");
    return 1;
    pinmodenoerror:

    pinMode(pin, mode);
    return 0;
} else if(!strcmp((char*)argv[0], "dwrite")){ //digitalwrite command: dwrite <pin> <lo | hi>
    uint pin = 2;
    uchar value = 0;
    if(argc < 3) goto dwriteerror; //number of arguments
    pin = atoi((char*)argv[1]);
    value = atoi((char*)argv[2]);
    if(value > 1)
        value = 1;
        goto dwritenoerror;
        dwriteerror:
        p("dwrite <pin> <lo | hi>\n");
        return 1;
        dwritenoerror:
    digitalWrite(pin, value);
    }
    else if(!strcmp((char*)argv[0], "dread")){ //digitalread command: dread <pin>
    uint pin = 0;
    uchar value = 0;
    if(argc < 2) goto dreaderror; //number of arguments
    pin = atoi((char*)argv[1]);

    goto dreadnoerror;
    dreaderror:
        p("dread <pin>\n");
        return 1;
    dreadnoerror:
    value = digitalRead(pin);
    p("pin %u = %s\n", pin, value == 0 ? "low" : "high");
    }

    else{
    p("command not recognized\n");
    return 1;
    }
return 0;
}

int mgmt_processcommand(uchar *cmdbuf){
cvector arguments_positions;
cvector arguments_sizes;
cvector_allocate(&arguments_positions, sizeof(uchar), 0);
cvector_allocate(&arguments_sizes, sizeof(uchar), 0);


uint i;
uchar lastpos = 0;

for(i=0;i<CMDBUFSIZE;i++){
uchar chari = i;

if(cmdbuf[i] == 0 || cmdbuf[i] == 0x20 || cmdbuf[i] == '\r' || cmdbuf[i] == '\n'){
cvector_push(&arguments_positions, &lastpos);
chari -= lastpos;
cvector_push(&arguments_sizes, &chari);
lastpos = i+1;
}

if(cmdbuf[i] == 0 || cmdbuf[i] == '\n' || cmdbuf[i] == '\r') break;

//cvector_push(&arguments_positions, &lastpos);
//cvector_push(&arguments_sizes, &chari);

}

//size sum
uchar totalsize = 0;
for(i=0;i<arguments_sizes.len;i++){
totalsize += *((uchar*)cvector_getitem(&arguments_sizes,i)) + 1;
}

uchar *tempbuff = (uchar*)malloc(totalsize + sizeof(uchar*)*arguments_sizes.len);
memset(tempbuff, 0, totalsize +  sizeof(uchar*)*arguments_sizes.len);
uchar **tempb;

for(i=0;i<arguments_positions.len;i++){
uchar templen = *((uchar*)cvector_getitem(&arguments_sizes, i));
uchar index = *((uchar*)cvector_getitem(&arguments_positions, i));
tempb = (uchar**)(tempbuff + totalsize);
tempb[i] = tempbuff + index;
memcpy(tempbuff + index, cmdbuf + index, templen);
}

commandprocessor(arguments_positions.len, tempb);

free(tempbuff);

cvector_destroy(&arguments_positions);
cvector_destroy(&arguments_sizes);
return 0;
}

int eventcheck(){
uchar mgmtconenable = digitalRead(mgmtconenablepin);
if(mgmtconenable == 1 && mgmtconlock == 0) //enable management console
{
Serial.write("Management console:\n");
mgmtconlock = 1;
return 1;
}

if(mgmtconlock == 1){ //if management console is enabled receive commands
while(Serial.available() > 0){
uchar receivedbyte = Serial.read();
mgmtcon_commandbuff[mgmtcon_commandbuffpos] = receivedbyte;
mgmtcon_commandbuffpos++;
if(receivedbyte == '\n' || mgmtcon_commandbuffpos == CMDBUFSIZE-1) {
mgmt_processcommand(mgmtcon_commandbuff);
memset(mgmtcon_commandbuff, 0, CMDBUFSIZE);
mgmtcon_commandbuffpos = 0;
}
}
}

return 0;
}

void loop()  {
eventcheck();
}
