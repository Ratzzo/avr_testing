#ifndef CVECTORH //guarded (:
#define CVECTORH


/* defines */
//#define CVECTOR_UPDATEONREALLOC //for speed, don't use this only if you know if realloc will return always the same address if the size is less
//#define CVECTOR_NOIF //this will remove the error checking, speeds it a bit, only use this if you know what are you doing
#define CVECTOR_LIGHTWEIGHT //define lightweight library, you have to use cvector_getitem instead of accessing directly to the items


typedef unsigned int uint; //uint
typedef unsigned char uchar; //uchar

typedef struct
{
#ifndef CVECTOR_LIGHTWEIGHT
    uchar *(*items); //items array
#endif
    uchar *itemsmem; //memory where the items are
    uint len; //length of the data
    uint itemsize; //length of each item in memory
} cvector;


#ifdef __cplusplus
extern "C" {
#endif
    extern uint cvector_allocate(cvector *vec, uint itemsize, uint length);
    extern uint cvector_setlength(cvector *vec, uint newlen);
    extern uint cvector_push(cvector *vec, void *data);
    extern uint cvector_pop(cvector *vec, void *data);
    extern uint cvector_insert(cvector *vec, void *data, uint index);
    extern uint cvector_delete(cvector *vec, uint index);
    extern uint cvector_xchg(cvector *vec, uint src, uint dest);
    extern uint cvector_destroy(cvector *vec); //destroy the vector
    extern uchar* cvector_getitem(cvector *vec, uint index);
    extern uint cvector_setitem(cvector *vec, void* in, uint index);
#ifdef __cplusplus
}
#endif

#endif // CVECTORH
