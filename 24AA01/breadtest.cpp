#include <arduino.h>
#include <Wire.h>
#include <24aa01.h>
const byte DEVADDR = 0x50;

//
// The display is like hexdump -C.  It will always
// begin and end on a 16-byte boundary.
//

void aa01_dump(byte devaddr, unsigned addr, unsigned length)
{
    // Start with the beginning of 16-bit page that contains the first byte
    unsigned startaddr = addr & (~0x0f);

    // stopaddr is address of next page after the last byte
    unsigned stopaddr  = (addr + length + 0x0f) & (~0x0f);

    for (unsigned i = startaddr; i < stopaddr; i += 16) {
        byte buffer[16]; // Hold a page of EEPROM
        char outbuf[6];  //Room for three hex digits and ':' and ' ' and '\0'
        sprintf(outbuf, "%03x: ", i);
        Serial.print(outbuf);
        aa01_read_buffer(devaddr, i, buffer, 16);
        for (int j = 0; j < 16; j++) {
            if (j == 8) {
                Serial.print(" ");
            }
            sprintf(outbuf, "%02x ", buffer[j]);
            Serial.print(outbuf);            
        }
        Serial.print(" |");
        for (int j = 0; j < 16; j++) {
            if (isprint(buffer[j])) {
                Serial.print(buffer[j]);
            }
            else {
                Serial.print('.');
            }
        }
        Serial.println("|");
    }
}

void setup()
{
    byte msg1[16] = {0};   // data to write
    byte msg2[] = {9,8,7,6,5,4,5,4,5,4, 0};
    byte msg3[] = {1,2,3,4,5,6,7,8,1,1,0};
    byte msgf[16] = {
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
    };
    
    
    Wire.begin();
    Serial.begin(9600);

    //
    // Change #if 0 to #if 1 and it will erase the
    // EEPROM pages that we are going to write to:
    //
    #if 1
      aa01_write_page(DEVADDR, 0x000, msgf, 16);
      aa01_write_page(DEVADDR, 0x010, msgf, 16);
      aa01_write_page(DEVADDR, 0x020, msgf, 16);
      aa01_write_page(DEVADDR, 0x030, msgf, 16);
      aa01_write_page(DEVADDR, 0x040, msgf, 16);
      aa01_write_page(DEVADDR, 0x050, msgf, 16);
      aa01_write_page(DEVADDR, 0x060, msgf, 16);
      aa01_write_page(DEVADDR, 0x070, msgf, 16);
      Serial.println("After erasing pages starting at 0x000, 0x100, and 0x1f0:");
      aa01_dump(DEVADDR, 0, 128);
    #endif

    //
    // Change #if 1 to #if 0 so that it won't write over the stuff next time
    //
    #if 1
    // Write some stuff to EEPROM
	int x;
	int y = 0;
	for(x=0;x<16;x++, y++){
	msg1[x] = y;
	}
    aa01_write_page(DEVADDR, 0x000, msg1, 16);
		for(x=0;x<16;x++, y++){
	msg1[x] = y;
	}
	aa01_write_page(DEVADDR, 0x010, msg1, 16);
    	for(x=0;x<16;x++, y++){
	msg1[x] = y;
	}
	aa01_write_page(DEVADDR, 0x020, msg1, 16);
		for(x=0;x<16;x++, y++){
	msg1[x] = y;
	}
	aa01_write_page(DEVADDR, 0x030, msg1, 16);
		for(x=0;x<16;x++, y++){
	msg1[x] = y;
	}
	aa01_write_page(DEVADDR, 0x040, msg1, 16);
		for(x=0;x<16;x++, y++){
	msg1[x] = y;
	}
	aa01_write_page(DEVADDR, 0x050, msg1, 16);
		for(x=0;x<16;x++, y++){
	msg1[x] = y;
	}
	aa01_write_page(DEVADDR, 0x060, msg1, 16);
		for(x=0;x<16;x++, y++){
	msg1[x] = y;
	}
	aa01_write_page(DEVADDR, 0x070, msg1, 16);
	#endif

    Serial.println("Memory written");
}

void loop()
{
    //
    // Read the first page in EEPROM memory, a byte at a time
    //
    Serial.println("aa01_read_byte, starting at 0");
    for (int i = 0; i < 16; i++) {
        byte b = aa01_read_byte(DEVADDR, i);
        Serial.print(b, HEX);
        Serial.print(' ');
    }
    Serial.println();
    
    //
    // Read the first page using the read_buffer function
    //
    Serial.println("aa01_read_buffer, starting at 0");
    byte buffer[16];
    aa01_read_buffer(DEVADDR, 0, buffer, sizeof(buffer));
    
    //
    //First print the hex bytes on this row
    //
    for (int i = 0; i < sizeof(buffer); i++) {
        char outbuf[6];
        sprintf(outbuf, "%02X ",buffer[i]);
        Serial.print(outbuf);
    }
    Serial.println();

    //
    // Now print the char if printable ASCII
    // otherwise print '.'
    //
    for (int i = 0; i < sizeof(buffer); i++) {
        if (isprint(buffer[i])) {
            Serial.print(buffer[i]);
        }
        else {
            Serial.print('.');
        }
    }
    Serial.println();
    
    // Now dump 512 bytes
    Serial.println("aa01_dump(DEVADDR, 0, 128)");
    aa01_dump(DEVADDR, 0, 128);
    Serial.println();

    delay(20000);

}