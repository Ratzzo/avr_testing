#include "PS2X_lib.h"  //for v1.6
#include "RF24.h"
#include "nRF24L01.h"
#include "printf.h"
#include <Arduino.h>
#include <avr/eeprom.h>
#include <avr/io.h>
#include <math.h>
#include <SPI.h>
#include "common.h"

#define PS2_DAT        5
#define PS2_CMD        6
#define PS2_SEL        7
#define PS2_CLK        8

#define pressures   true
#define rumble      false

#define BOOTUP_PIN  9
#define TEST_PIN A0

PS2X ps2x;

RF24 rf(A1,4);

int error = 0;
byte type = 0;
byte vibrate = 0;
uint32_t lastmillis = 0;
uint32_t startmillis = 0;
uint32_t ms = 0;
uint8_t initialized = 0; //once initialized we can use the indicator leds for other purpose such as RF connection
uint8_t led1_state = 0;
uint8_t led2_state = 0;

const uint64_t baseaddr = 0xF0F0F0F000LL;
uint64_t device_address = 0;

void setup()
{
//    OCR1A = 1;
    Serial.begin(115200);
    DDRB |= _BV(DDB6);
    DDRB |= _BV(DDB7);
    DDRC |= _BV(DDC0);
    DDRC |= _BV(DDC1);
    delay(300);
    printf_begin();
    uint8_t subaddr;
    eeprom_read_block((void*)&subaddr, 0, sizeof(uint8_t));
    device_address = baseaddr + subaddr;

        //confirm bootup with led


    //initialize RF
    uint8_t rf_val = rf.begin();
    printf("rf.begin(); == %u", rf_val);
    if(!rf_val){
        rf.setRetries(15,15);
        rf.setPayloadSize(8);
        //rf.openWritingPipe(pipes[1]);
        rf.openReadingPipe(1,device_address);
        rf.startListening();
        rf.printDetails();
        PORTB |= _BV(PB6);

    }
    else
        initialized = 2;


    pinMode(BOOTUP_PIN, OUTPUT);
    //PORTB |= _BV(PB6);
    lastmillis = millis();
    startmillis = lastmillis;
    ms = lastmillis;

    //blah analog receiver shit
    pinMode(10, OUTPUT);
    pinMode(6, OUTPUT);
    pinMode(5, OUTPUT);
    pinMode(3, OUTPUT);
}

int ledstate = 0;
float b = 0;

uint16_t lastbuttondata = 0;
uint32_t laststickdata = 0;

void loop()
{
    //glow to indicate process running
    if(millis() > ms + 10){
        double val;
        b += 0.01;
        val = 127 - 127*sin(b);
        analogWrite(BOOTUP_PIN, val);
        ms = millis();
        if(b >= PI) b = 0;
	}

    if(initialized == 2)
        return;
    //turn off leds after some time passed
    if(!initialized && millis() > startmillis + 500){
        PORTB &= ~_BV(PB6);
        initialized = 1;
    }


	if(initialized){


        //blink led to indicate data received
        if(millis() > lastmillis + 10){
            PORTB &= ~_BV(PB6);
        }

        if(rf.available()){
            uint8_t buffer[8];
            uint8_t *bufferptr = buffer;

            uint16_t buttondata = 0;
            int done = 0;
            while(!done){

            done = rf.read(&buffer, sizeof(buffer));

            //parse buffer
            switch(*(bufferptr++)){
                case DATA_TYPE_BUTTON:
                {
                    buttondata = *((uint16_t*)bufferptr);
                    if(lastbuttondata != buttondata){
                        //buttons changed
                        PORTB |= _BV(PB6);
                        lastbuttondata = buttondata;
                        lastmillis = millis();


                        //parse button pressed
                        /*
                        if(!(buttondata & PSB_PAD_DOWN)){
                            PORTC |= _BV(PC0);
                        } else {
                            PORTC &= ~_BV(PC0);
                        }
                        if(!(buttondata & PSB_PAD_UP)){
                            PORTC |= _BV(PC1);
                        } else {
                            PORTC &= ~_BV(PC1);
                        } */
                    }
                    break;
                }
                case DATA_TYPE_STICK_ANALOG:
                {
                    uint32_t stickdata = *((uint32_t*)bufferptr);
                    if(laststickdata != stickdata){
                        PORTB |= _BV(PB6);
                        laststickdata = stickdata;
                        lastmillis = millis();
                        uint8_t lx = *bufferptr++;
                        uint8_t ly = *bufferptr++;
                        uint8_t rx = *bufferptr++;
                        uint8_t ry = *bufferptr++;
                        analogWrite(10, lx);
                        analogWrite(6, ly);
                        analogWrite(5, rx);
                        analogWrite(3, ry);



                        //parse stick data

                    }
                    break;
                }
            }


            }
        }
	}


//  delay(50);
}
