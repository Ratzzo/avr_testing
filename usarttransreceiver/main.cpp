#undef F_CPU
#define F_CPU 8000000UL
#include <Arduino.h>
#include <avr/io.h>
#include <stdarg.h>


#undef printf
void p(char *fmt, ... ){
        char tmp[128]; // resulting string limited to 128 chars
        va_list args;
        va_start (args, fmt );
        vsnprintf(tmp, 128, fmt, args);
        va_end (args);
        Serial.print(tmp);
}

static const unsigned char key[28] = {
0,0,0,0,
0,0,0,0,
0,0,0,0,
0,0,0,0,
0,0,0,0,
0,0,0,0,
0,0,0,0};

void setup()
{
	Serial.begin(9600);
    DDRB |= (1<<DDB5); //output
    DDRB |= (1<<DDB4); //output
    PORTB |= (1<<PORTB5); //set to 1
}

unsigned long m = 0, lastm = 0;

void loop()
{
m = millis();

unsigned char inout[] = "\0\0\0\0\0\0\0\0";


//BF_set_key(&blowfish_key, 28, key);

//p((char*)"key error %u",  ERR_get_error());

int i = 0;
for(i=0;i<8;i++)
p("%2X ", inout[i]);
p("\n\n");

//BF_encrypt((long unsigned int*)inout, &blowfish_key);

for(i=0;i<8;i++)
p("%2X ", inout[i]);
p("\n\n");

//BF_decrypt((long unsigned int*)inout, &blowfish_key);

for(i=0;i<8;i++)
p("%2X ", inout[i]);
p("\n\n");
p("############################\n\n");
delay(1000);
if(m - lastm >= 1000){
lastm = m;
}
}
