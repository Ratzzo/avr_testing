#define F_CPU 8000000UL
#include <Arduino.h>

//#include <util/delay.h>
#include <avr/io.h>
/*
  Turns on an LED on for one second, then off for one second, repeatedly.
*/

void setup()
{
//	Serial.begin(9600);
    DDRB |= (1<<DDB5); //output
    DDRB |= (1<<DDB4); //output
PORTB &= ~(1<<PORTB5); //set to 1
}

void loop()
{
PORTB ^= (1<<PORTB5); //turn off
unsigned long int i;
for(i=0;i<0xFFFFFFUL;i++)
PORTB ^= (1<<PORTB4);
PORTB ^= (1<<PORTB5); //turn on again
delay(100);
}
