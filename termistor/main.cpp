#include <Arduino.h>
#include "../lib/twi/twi.h"
//#include <SPI.h>
#include "../lib/mcp23017/MCP23017.h"
#include "../lib/mcp23017/lcd.h"
#include <avr/io.h>

mcp23017_t mcp0;


MCPLiquidCrystal *lcd;



void setup()  {
DDRB |= (1<<DDB0);
PORTB |= (1<<PORTB0);
twi_init(400000L);
pinMode(A0, INPUT);
pinMode(A1, INPUT);
mcp23017_create(&mcp0, 0);
mcp23017_setbyte(&mcp0, MCP23017ADDR_IODIRB, 0x00);
mcp23017_setbyte(&mcp0, MCP23017ADDR_GPIOB, 0x00 | 1 << 7);
//MCPLiquidCrystal dlcd(&mcp0, MCP23017ADDR_GPIOA, 0, 1, 2, 3, 4, 5, 6);
lcd = new MCPLiquidCrystal(&mcp0, MCP23017ADDR_GPIOB, 0, 1, 2, 3, 4, 5, 6);
  // set up the LCD's number of columns and rows:
  lcd->begin(16, 2);
  // Print a message to the LCD.
  //pinMode(3, OUTPUT);
//while(1){

//}
}

char arr[128];

double analogTemperature(double x){
	double a = 0.004737873442f;
	double b = 1.844707633f;
	double c = 194.3001616f;
	
	//calculate using the polynomial regression formula
	
	double result = x*x*a - x*b + c;
	
	return result;
}

void loop()  {
	lcd->clear();
	sprintf(arr, "%i", (int)(10*analogTemperature(analogRead(A1))) 	);
	lcd->setCursor(0,0);
	lcd->print(arr);
	delay(100);

/*uchar bytestosend[2];
bytestosend[0] = MCP23017ADDR_IODIRA;//register addr
bytestosend[1] = 0x00; //all bits as output
twi_writeTo(MCP23017_FIXEDADDR | 0, bytestosend, 2, 0, 1); //send and stop
bytestosend[0] = MCP23017ADDR_IODIRB;//register addr
bytestosend[1] = 0x00; //all bits as output
twi_writeTo(MCP23017_FIXEDADDR | 0, bytestosend, 2, 0, 1); //send and stop
bytestosend[0] = MCP23017ADDR_GPIOA;//register addr
bytestosend[1] = ((char*)&x)[0]; //all bits as output
twi_writeTo(mcp0.raddress, bytestosend, 2, 0, 1); //send and stop
bytestosend[0] = MCP23017ADDR_GPIOB;//register addr
bytestosend[1] = ((char*)&x)[1]; //all bits as output
twi_writeTo(MCP23017_FIXEDADDR | 0, bytestosend, 2, 0, 1); //send and stop
*/
//mcp23017_setbyte(&mcp0, MCP23017ADDR_IODIRA, 0x00);

//mcp23017_setbyte(&mcp0, MCP23017ADDR_IODIRA, 0x00);
//mcp23017_setbyte(&mcp0, MCP23017ADDR_IODIRB, 0x00);
//mcp23017_setbyte(&mcp0, MCP23017ADDR_GPIOA, 0x00 | 1 << 7);
//mcp23017_setbyte(&mcp0, MCP23017ADDR_GPIOB, 0x00 | 1);
//mcp23017_setbyte(&mcp0, MCP23017ADDR_GPIOA, ((char*)&x)[0]);
/*
mcp23017_setbyte(&mcp0, MCP23017ADDR_IODIRA, 0x00);
x = mcp23017_getbyte(&mcp0, MCP23017ADDR_GPIOA);
delay(200);
mcp23017_setbyte(&mcp0, MCP23017ADDR_GPIOA, 0);
*/
//PORTB ^= (1<<PORTB5);
//uchar bytestosend[2];
//bytestosend[0] = MCP23017ADDR_GPIOA;//register addr
//bytestosend[1] = x; //all bits as output
//twi_writeTo(MCP23017_FIXEDADDR | 0, bytestosend, 2, 0, 1); //send and stop
//delay(100);
}
