#include <Arduino.h>
#include <math.h>
#include <stdint.h>
#include <mcp23017.h>
#include <twi.h>
#include "keypad.h"
#include "int.h"

#define LED 13
#define SINEPWM 5

unsigned long ms = 0;
unsigned long ms2 = 0;
unsigned long ms3 = 0;
unsigned long refreshms = 0;
mcp23017_keypad_t kp;

volatile bool keypressed = 0;

void setup()  {
	twi_init(400000L);
	Serial.begin(115200);
	pinMode(LED, OUTPUT);
	pinMode(9, INPUT);
	pinMode(SINEPWM, OUTPUT);
	ms = millis();
	ms2 = millis();
	ms3 = millis();
	refreshms = millis();
	delay(100); //wait for mcp to power up
	mcp23017_keypad_create(&kp, KP_PORTA, 0x00);
	//mcp23017_KeyPad(KP_PORTA, 0x00);
}

volatile int inc = 0;


bool i = 0;
int32_t x = 0;
float b = 0;

uint8_t out = 0;

double val = 0;

uint16_t lastl = 0;
bool keylock = 0;

void keyhandler(uint16_t key){
	Serial.println(key);
}

#define KEYCOUNTER_MAX 100

void loop()  {
	if(!digitalRead(9)){
		keylock = 1;
		refreshms = millis();
		uint16_t i = mcp23017_keypad_checkForInput(&kp);
		if(i != lastl && i)
			keyhandler(i);
		lastl = i;
	}
	if(millis() > refreshms + 100 && keylock){
		keylock = 0;
		lastl = 0;
	}/*
	if(millis() > ms + 10){
	b += 0.01;
	val = 127 + 127*sin(b);
		analogWrite(SINEPWM, val);
		//digitalWrite(5, 1);
		ms = millis();
	}
	
	if(millis() > ms3 + 500){
	//Serial.println(mcp23017_keypad_checkForInput(&kp));
	//	uint16_t l = mcp23017_keypad_checkForInput(&kp);
	//	Serial.println(l);
		ms3 = millis();
	}
	if(millis() > ms2 + 1000){
		//Serial.println(val);
		ms2 = millis();
		digitalWrite(LED, i ^= 1);
			//mcp23017_setbyte(&kp->mcp, MCP23017ADDR_IODIRA, 0x00);
			//mcp23017_setbyte(&kp->mcp, MCP23017ADDR_GPIOA, out);
			//out ^= 0xff;
	}
	
	*/
}
