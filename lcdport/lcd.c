#include "lcd.h"

#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include "Arduino.h"

// When the display powers up, it is configured as follows:
//
// 1. Display clear
// 2. Function set:
//    DL = 1; 8-bit interface data
//    N = 0; 1-line display
//    F = 0; 5x8 dot character font
// 3. Display on/off control:
//    D = 0; Display off
//    C = 0; Cursor off
//    B = 0; Blinking off
// 4. Entry mode set:
//    I/D = 1; Increment by 1
//    S = 0; No shift
//
// Note, however, that resetting the Arduino doesn't reset the LCD, so we
// can't assume that its in that state when a sketch starts (and the
// LiquidCrystal constructor is called).

void mcplcd_create(mcplcd_t lcd, mcp23017_t *master, uchar bank, uint8_t rs, uint8_t rw, uint8_t enable,
			     uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3,
			     uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7)
{
_master = master;
_bank = bank;

 mcplcd_init(lcd, 0, rs, rw, enable, d0, d1, d2, d3, d4, d5, d6, d7);
}

void mcplcd_create1(mcplcd_t lcd, mcp23017_t *master, uchar bank, uint8_t rs, uint8_t enable,
			     uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3,
			     uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7)
{
    _master = master;
_bank = bank;
 mcplcd_init(lcd, 0, rs, 255, enable, d0, d1, d2, d3, d4, d5, d6, d7);
}

void mcplcd_create2(mcplcd_t lcd, mcp23017_t *master, uchar bank, uint8_t rs, uint8_t rw, uint8_t enable,
			     uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3)
{
    _master = master;
_bank = bank;
 mcplcd_init(lcd, 1, rs, rw, enable, d0, d1, d2, d3, 0, 0, 0, 0);
}

void mcplcd_create(mcplcd_t lcd, mcp23017_t *master, uchar bank, uint8_t rs,  uint8_t enable,
			     uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3)
{
    _master = master;
_bank = bank;
 mcplcd_init(lcd, 1, rs, 255, enable, d0, d1, d2, d3, 0, 0, 0, 0);
}

void mcplcd_init(mcplcd_t lcd, uint8_t fourbitmode, uint8_t rs, uint8_t rw, uint8_t enable,
			 uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3,
			 uint8_t d4, uint8_t d5, uint8_t d6, uint8_t d7)
{
  _rs_pin = rs;
  _rw_pin = rw;
  _enable_pin = enable;

  _iodir = _bank == MCP23017ADDR_GPIOA ? MCP23017ADDR_IODIRA : MCP23017ADDR_IODIRB;


  _data_pins[0] = d0;
  _data_pins[1] = d1;
  _data_pins[2] = d2;
  _data_pins[3] = d3;
  _data_pins[4] = d4;
  _data_pins[5] = d5;
  _data_pins[6] = d6;
  _data_pins[7] = d7;

 mcplcd_pinMode(lcd, _rs_pin, OUTPUT);
  // we can save 1 pin by not using RW. Indicate by passing 255 instead of pin#
  if (_rw_pin != 255) {
   mcplcd_pinMode(lcd, _rw_pin, OUTPUT);
  }
 mcplcd_pinMode(lcd, _enable_pin, OUTPUT);

  if (fourbitmode)
    _displayfunction = LCD_4BITMODE | LCD_1LINE | LCD_5x8DOTS;
  else
    _displayfunction = LCD_8BITMODE | LCD_1LINE | LCD_5x8DOTS;

 mcplcd_begin(lcd, 16, 1);
}

void mcplcd_begin(mcplcd_t lcd, uint8_t cols, uint8_t lines, uint8_t dotsize) {
  if (lines > 1) {
    _displayfunction |= LCD_2LINE;
  }
  _numlines = lines;
  _currline = 0;

  // for some 1 line displays you can select a 10 pixel high font
  if ((dotsize != 0) && (lines == 1)) {
    _displayfunction |= LCD_5x10DOTS;
  }

  // SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
  // according to datasheet, we need at least 40ms after power rises above 2.7V
  // before sending commands. Arduino can turn on way befer 4.5V so we'll wait 50
 mcplcd_delayMicroseconds(lcd, 50000);
  // Now we pull both RS and R/W low to begin commands
 mcplcd_digitalWrite(lcd, _rs_pin, LOW);
 mcplcd_digitalWrite(lcd, _enable_pin, LOW);
  if (_rw_pin != 255) {
   mcplcd_digitalWrite(lcd, _rw_pin, LOW);
  }

  //put the LCD into 4 bit or 8 bit mode
  if (! (_displayfunction & LCD_8BITMODE)) {
    // this is according to the hitachi HD44780 datasheet
    // figure 24, pg 46

    // we start in 8bit mode, try to set 4 bit mode
    write4bits(0x03);
   mcplcd_delayMicroseconds(lcd, 4500); // wait min 4.1ms

    // second try
    write4bits(0x03);
   mcplcd_delayMicroseconds(lcd, 4500); // wait min 4.1ms

    // third go!
    write4bits(0x03);
   mcplcd_delayMicroseconds(lcd, 150);

    // finally, set to 4-bit interface
    write4bits(0x02);
  } else {
    // this is according to the hitachi HD44780 datasheet
    // page 45 figure 23

    // Send function set command sequence
   mcplcd_command(lcd, LCD_FUNCTIONSET | _displayfunction);
   mcplcd_delayMicroseconds(lcd, 4500);  // wait more than 4.1ms

    // second try
   mcplcd_command(lcd, LCD_FUNCTIONSET | _displayfunction);
   mcplcd_delayMicroseconds(lcd, 150);

    // third go
   mcplcd_command(lcd, LCD_FUNCTIONSET | _displayfunction);
  }

  // finally, set # lines, font size, etc.
 mcplcd_command(lcd, LCD_FUNCTIONSET | _displayfunction);

  // turn the display on with no cursor or blinking default
  _displaycontrol = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;
 mcplcd_display(lcd, );

  // clear it off
 mcplcd_clear(lcd, );

  // Initialize to default text direction (for romance languages)
  _displaymode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
  // set the entry mode
 mcplcd_command(lcd, LCD_ENTRYMODESET | _displaymode);

}

/********** high level commands, for the user! */
void mcplcd_clear(mcplcd_t lcd, )
{
 mcplcd_command(lcd, LCD_CLEARDISPLAY);  // clear display, set cursor position to zero
 mcplcd_delayMicroseconds(lcd, 2000);  // this command takes a long time!
}

void mcplcd_home(mcplcd_t lcd, )
{
 mcplcd_command(lcd, LCD_RETURNHOME);  // set cursor position to zero
 mcplcd_delayMicroseconds(lcd, 2000);  // this command takes a long time!
}

void mcplcd_setCursor(mcplcd_t lcd, uint8_t col, uint8_t row)
{
  int row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };
  if ( row >= _numlines ) {
    row = _numlines-1;    // we count rows starting w/0
  }

 mcplcd_command(lcd, LCD_SETDDRAMADDR | (col + row_offsets[row]));
}

// Turn the display on/off (quickly)
void mcplcd_noDisplay(mcplcd_t lcd, ) {
  _displaycontrol &= ~LCD_DISPLAYON;
 mcplcd_command(lcd, LCD_DISPLAYCONTROL | _displaycontrol);
}
void mcplcd_display(mcplcd_t lcd, ) {
  _displaycontrol |= LCD_DISPLAYON;
 mcplcd_command(lcd, LCD_DISPLAYCONTROL | _displaycontrol);
}

// Turns the underline cursor on/off
void mcplcd_noCursor(mcplcd_t lcd, ) {
  _displaycontrol &= ~LCD_CURSORON;
 mcplcd_command(lcd, LCD_DISPLAYCONTROL | _displaycontrol);
}
void mcplcd_cursor(mcplcd_t lcd, ) {
  _displaycontrol |= LCD_CURSORON;
 mcplcd_command(lcd, LCD_DISPLAYCONTROL | _displaycontrol);
}

// Turn on and off the blinking cursor
void mcplcd_noBlink(mcplcd_t lcd, ) {
  _displaycontrol &= ~LCD_BLINKON;
 mcplcd_command(lcd, LCD_DISPLAYCONTROL | _displaycontrol);
}
void mcplcd_blink(mcplcd_t lcd, ) {
  _displaycontrol |= LCD_BLINKON;
 mcplcd_command(lcd, LCD_DISPLAYCONTROL | _displaycontrol);
}

// These commands scroll the display without changing the RAM
void mcplcd_scrollDisplayLeft(mcplcd_t lcd, void) {
 mcplcd_command(lcd, LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}
void mcplcd_scrollDisplayRight(mcplcd_t lcd, void) {
 mcplcd_command(lcd, LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

// This is for text that flows Left to Right
void mcplcd_leftToRight(mcplcd_t lcd, void) {
  _displaymode |= LCD_ENTRYLEFT;
 mcplcd_command(lcd, LCD_ENTRYMODESET | _displaymode);
}

// This is for text that flows Right to Left
void mcplcd_rightToLeft(mcplcd_t lcd, void) {
  _displaymode &= ~LCD_ENTRYLEFT;
 mcplcd_command(lcd, LCD_ENTRYMODESET | _displaymode);
}

// This will 'right justify' text from the cursor
void mcplcd_autoscroll(mcplcd_t lcd, void) {
  _displaymode |= LCD_ENTRYSHIFTINCREMENT;
 mcplcd_command(lcd, LCD_ENTRYMODESET | _displaymode);
}

// This will 'left justify' text from the cursor
void mcplcd_noAutoscroll(mcplcd_t lcd, void) {
  _displaymode &= ~LCD_ENTRYSHIFTINCREMENT;
 mcplcd_command(lcd, LCD_ENTRYMODESET | _displaymode);
}

// Allows us to fill the first 8 CGRAM locations
// with custom characters
void mcplcd_createChar(mcplcd_t lcd, uint8_t location, uint8_t charmap[]) {
  location &= 0x7; // we only have 8 locations 0-7
 mcplcd_command(lcd, LCD_SETCGRAMADDR | (location << 3));
  for (int i=0; i<8; i++) {
   mcplcd_write(lcd, charmap[i]);
  }
}

/*********** mid level commands, for sending data/cmds */

inline void mcplcd_command(mcplcd_t lcd, uint8_t value) {
 mcplcd_send(lcd, value, LOW);
}

inline size_t mcplcd_write(mcplcd_t lcd, uint8_t value) {
 mcplcd_send(lcd, value, HIGH);
  return 1; // assume sucess
}

inline size_t mcplcd_printf(){
	
}

/************ low level data pushing commands **********/

// write either command or data, with automatic 4/8-bit selection

void mcplcd_digitalWrite(mcplcd_t lcd, uint8_t value, uint8_t mode) {
uchar byte = mcp23017_getbyte(_master, _bank);
byte &= ~(1 << value);
byte |= (mode << value);
mcp23017_setbyte(_master, _bank, byte);
}

void mcplcd_pinMode(mcplcd_t lcd, uint8_t value, uint8_t mode) {
uchar byte = mcp23017_getbyte(_master, _iodir);
byte &= ~(1 << value);
byte |= (!mode << value);
mcp23017_setbyte(_master, _iodir, byte);
}

void mcplcd_send(mcplcd_t lcd, uint8_t value, uint8_t mode) {
 mcplcd_digitalWrite(lcd, _rs_pin, mode);

  // if there is a RW pin indicated, set it low to Write
  if (_rw_pin != 255) {
   mcplcd_digitalWrite(lcd, _rw_pin, LOW);
  }

  if (_displayfunction & LCD_8BITMODE) {
    write8bits(value);
  } else {
    write4bits(value>>4);
    write4bits(value);
  }
}

void mcplcd_pulseEnable(mcplcd_t lcd, void) {
 mcplcd_digitalWrite(lcd, _enable_pin, LOW);
 mcplcd_delayMicroseconds(lcd, 1);
 mcplcd_digitalWrite(lcd, _enable_pin, HIGH);
 mcplcd_delayMicroseconds(lcd, 1);    // enable pulse must be >450ns
 mcplcd_digitalWrite(lcd, _enable_pin, LOW);
 mcplcd_delayMicroseconds(lcd, 100);   // commands need > 37us to settle
}

void mcplcd_write4bits(uint8_t value) {
  for (int i = 0; i < 4; i++) {
   mcplcd_pinMode(lcd, _data_pins[i], OUTPUT);
   mcplcd_digitalWrite(lcd, _data_pins[i], (value >> i) & 0x01);
  }

 mcplcd_pulseEnable(lcd, );
}

void mcplcd_write8bits(uint8_t value) {
  for (int i = 0; i < 8; i++) {
   mcplcd_pinMode(lcd, _data_pins[i], OUTPUT);
   mcplcd_digitalWrite(lcd, _data_pins[i], (value >> i) & 0x01);
  }

 mcplcd_pulseEnable(lcd, );
}
